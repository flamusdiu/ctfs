# Task 1

## Overview

Having issues, found an error, or think something isn't quite right?

Hit us up on Twitter with [@OSPASafeEscape][1]

RULES:

This CTF is designed to be for all skill levels.  Hints may or may not be provided.  All answers must be in lowercase, grammar (commas) and spelling count, and spaces may be applicable as well.

Tools you may or may not need:

Base64 Encoding - [https://www.base64decode.org][2]

HEX Editor - [https://hexed.it][3]

DiskDrill - [https://www.anyrecover.com/data-recovery/?media=googleppc&gclid=CjwKCAjwruSHBhAtEiwA_qCppr5ZtEL4fHqcbqXu76KsMsCfbBrtezK4vwcZEac5Pu7UHR-WOkxzJBoC3S4QAvD_BwE][4]

---

Photo for Question #1:

![photo #1][5]

## Answer the questions below

1. ![x-red] In photo #1 above, who do I belong too? (Full Name)
2. ![x-red] When is the most likely time that domestic violence takes place?
3. ![check] More than ___________ phone calls are placed every day to domestic violence hotlines nationwide. : **20,000**
   1. [OSE Facts][9]
4. ![check] When was OSE founded? (Year) : **2016**
   2. [About Operation Safe Escape][6] under Quick Facts
5. ![check] Professor Chaos was last seen near 37.8199° N, 122.4783° W - Do you know what this may be?
   3. Searching Google Maps with the lat/long: [Google Maps: Golden Gate Bridge][7]
   4. <br /> ![Screenshot: Google Maps @ Golden Gate][8]

[1]: https://twitter/OSPASafeEscape "@OSPASafeEscape"
[2]: https://www.base64decode.org "Base64Decode"
[3]: https://hexed.it "Hex Editor"
[4]: https://www.anyrecover.com/data-recovery/?media=googleppc&gclid=CjwKCAjwruSHBhAtEiwA_qCppr5ZtEL4fHqcbqXu76KsMsCfbBrtezK4vwcZEac5Pu7UHR-WOkxzJBoC3S4QAvD_BwE "DiskDrill"
[5]: ../file/b9af33177c2710bdcdd4a55ca624cec5.jpg "Photo #1"
[6]: https://safeescape.org/about-us/ "About Operation Safe Escape"
[7]: https://www.google.com/maps/place/37%C2%B049'11.6%22N+122%C2%B028'41.9%22W/@37.8199042,-122.4804887,17z/data=!3m1!4b1!4m5!3m4!1s0x0:0x0!8m2!3d37.8199!4d-122.4783 "Google Maps: Golden Gate Bridge"
[8]: images/google-maps-golden-gate.png "Screenshot: Google Maps @ Golden Gate"
[9]: https://safeescape.org/facts/ "OSE Facts"
[check]: images/checkmark.png "Checkmark"
[x-red]: images/x-red.png "Red X"
[yield]: images/yield.png "Yield"
[question-mark]: images/question-mark.png "Question Mark"