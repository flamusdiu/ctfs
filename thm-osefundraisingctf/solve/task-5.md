# Task 5

## Overview

Having issues, found an error, or think something isn't quite right?

Hit us up on Twitter with [@OSPASafeEscape][1]

RULES:

This CTF is designed to be for all skill levels.  Hints may or may not be provided.  All answers must be in lowercase, grammar (commas) and spelling count, and spaces may be applicable as well.

Tools you may or may not need:

Base64 Encoding - [https://www.base64decode.org][2]

HEX Editor - [https://hexed.it][3]

DiskDrill - [https://www.anyrecover.com/data-recovery/?media=googleppc&gclid=CjwKCAjwruSHBhAtEiwA_qCppr5ZtEL4fHqcbqXu76KsMsCfbBrtezK4vwcZEac5Pu7UHR-WOkxzJBoC3S4QAvD_BwE][4]

---

Google Drive link for Question #5 (AOLboot.exe):

_**Deleted**_ &lsqb;https&colon;&sol;&sol;drive&period;google&period;com&sol;file&sol;d&sol;1rJHIna2s7-RYTKGAypUjqQj0u0TEnLEv&sol;view&quest;usp&equals;sharing&rsqb;

**Note**: Most of the questions seem to be related to this execuable which was deleted and such cannot be answered.

Photo for Question #2:
![Photo #1][4]

## Answer the questions below

1. ![red-x] Where are the kidnappers holding the victims?
2. ![red-x] Best date ever!  Said only one person - Who was this?  (Refer to PHOTO #1 in the description above)
3. ![check]19.3 Million women and 5.1 million men in the US have been the victims of this type of domestic violence... : **stalking**
   1. [National Intimate Partner and Sexual Violence Survey: 2015 Data Brief][7]
4. ![question-mark] From aol_boot.exe what type of volumes were? (See above description for Google Drive link to the file)
5. ![yield] uutasymssat ollemrdmnr aes ur mbkio
   1. This one is a [polyalphbetic cypher][5].
   2. Using [Kasiski's test][6], the length of the key might be either 5 or 6 letters.
   3. The key _**might**_ be located in the missing executable.
6. ![question-mark] Intimate partner violence accounts for _____ of all violent crime.
7. ![question-mark] What flag do you have with coffee in the aol_boot.exe?
8. ![question-mark] How many files were in the container? (aol_boot)
9. ![question-mark] What was the flag to get all the brownies?
10. ![question-mark] What was the name of the closeup picture?
11. ![question-mark] What type of food was not for humans?

[1]: https://twitter/OSPASafeEscape "@OSPASafeEscape"
[2]: https://www.base64decode.org "Base64Decode"
[3]: https://hexed.it "Hex Editor"
[4]: ../file/9d8293e036f7b0b3e2fb9987ac5bd696.jpg "Photo #1"
[5]: https://en.wikipedia.org/wiki/Polyalphabetic_cipher#:~:text=A%20polyalphabetic%20cipher%20is%20any,is%20a%20simplified%20special%20case. "Polyalphabetic cipher"
[6]: https://en.wikipedia.org/wiki/Kasiski_examination "Kasiski examination"
[7]: https://www.cdc.gov/violenceprevention/datasources/nisvs/2015NISVSdatabrief.html "National Intimate Partner and Sexual Violence Survey: 2015 Data Brief"
[check]: images/checkmark.png "Checkmark"
[x-red]: images/x-red.png "Red X"
[yield]: images/yield.png "Yield"
[question-mark]: images/question-mark.png "Question Mark"
