# Task 3

## Overview

Having issues, found an error, or think something isn't quite right?

Hit us up on Twitter with [@OSPASafeEscape][1]

RULES:

This CTF is designed to be for all skill levels.  Hints may or may not be provided.  All answers must be in lowercase, grammar (commas) and spelling count, and spaces may be applicable as well.

Tools you may or may not need:

Base64 Encoding - [https://www.base64decode.org][2]

HEX Editor - [https://hexed.it][3]

DiskDrill - [https://www.anyrecover.com/data-recovery/?media=googleppc&gclid=CjwKCAjwruSHBhAtEiwA_qCppr5ZtEL4fHqcbqXu76KsMsCfbBrtezK4vwcZEac5Pu7UHR-WOkxzJBoC3S4QAvD_BwE][4]

---

Photo for Question #5:

PHOTO #1

![Photo #1][4]

## Answer the questions below

1. ![check] I am most likely to form a hamster army, who am I? : **lesley carhart**
   1. Searching for each "kindnapped person", I find <br /> ![Google Search - Lesley Carhart hamster][6]
2. ![check] This document is intended to prohibit your partner from physically coming near you or harming or harassing you, your children, or other loved ones. : **restraining orders**
   1. [Domestic Violence Restraining Orders][5]
3. ![check] Which State has the highest instances of domestic violence (Both men and women)? : **oklahoma**
   1. [Domestic Violence Statistics by State][13]
4. ![check] _____ people per minute on average are physically abused by an intimate partner.  This equates to more than 10 million women and men. : **20**
   1. [NCAVD National Statistics][14]
5. ![yield] These shoes are FIRE!!!!!  Who wished they had found these in 2020? (Refer to PHOTO #1 in the description above)
   1. At first glance, the shoes appear to be [Vans][12] and usually known to be worn by skateboarders. Though, the usual reverse images search did not work.
   2. Using [Google Lens][11], you can easily get a hit on which types of shoes these are. <br /> ![Google Lens - VANS Shoes][10] <br />Vans Samurai Rising Era Shoes
6. ![check] In his best Professor Chaos voice - "I hear Chicago is a great place to visit, know what state I'm in?" : **indiana**
   1. From the question, I went and tried the most obvious, "illinois," which did not work.
   2. Counting the letters, the state had 7 letters in its name.
   3. Grabbed a [list of states in alphabetical order][9] and put that into Google Sheets.
   4. In Google Sheets, I counted the letters in each state: `=len(A1)` <br /> ![Google Sheets - States][8]
   5. If you look at Google maps, or just know,that "Indiana" is the closest state <br /> ![Google Maps - Chicago][7]

[1]: https://twitter/OSPASafeEscape "@OSPASafeEscape"
[2]: https://www.base64decode.org "Base64Decode"
[3]: https://hexed.it "Hex Editor"
[4]: ../file/123802b3007369a6f5905115a8eb8214.jpg "Photo #1"
[5]: https://www.womenslaw.org/laws/general/restraining-orders "Domestic Violence Restraining Orders"
[6]: images/google-search-hamsters.png "Google Search - Lesley Carhart hamster"
[7]: images/google-maps-chicago.png "Google Maps - Chicago"
[8]: images/google-sheets-states.png "Google Sheets - States"
[9]: https://alphabetizer.flap.tv/lists/list-of-states-in-alphabetical-order.php "List of States in Alphabetical Order"
[10]: images/vans-samurai-rising-shoes.jpg "Google Lens - VANS Shoes"
[11]: https://lens.google/ "Google Lens"
[12]: https://www.vans.com/ "VANS"
[13]: https://worldpopulationreview.com/state-rankings/domestic-violence-by-state "Domestic Violence Statistics by State"
[14]: https://ncadv.org/statistics "NCAVD National Statistics"
[check]: images/checkmark.png "Checkmark"
[x-red]: images/x-red.png "Red X"
[yield]: images/yield.png "Yield"
[question-mark]: images/question-mark.png "Question Mark"
