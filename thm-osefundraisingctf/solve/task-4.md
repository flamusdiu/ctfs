# Task 4

## Overview

Having issues, found an error, or think something isn't quite right?

Hit us up on Twitter with [@OSPASafeEscape][1]

RULES:

This CTF is designed to be for all skill levels.  Hints may or may not be provided.  All answers must be in lowercase, grammar (commas) and spelling count, and spaces may be applicable as well.

Tools you may or may not need:

Base64 Encoding - [https://www.base64decode.org][2]

HEX Editor - [https://hexed.it][3]

DiskDrill - [https://www.anyrecover.com/data-recovery/?media=googleppc&gclid=CjwKCAjwruSHBhAtEiwA_qCppr5ZtEL4fHqcbqXu76KsMsCfbBrtezK4vwcZEac5Pu7UHR-WOkxzJBoC3S4QAvD_BwE][4]

---

PHOTO #1

![Photo #1][4]

## Answer the questions below

1. ![check] ...and I quote, "Is this final boss enough for you?" - Who am I? (Refer to PHOTO #1 in the description above)
2. I am a form of software that when installed on a target device gives a user the ability to exert control over their victims.  I can track their location, intercept phone calls, text messages, and email, eavesdrop on phone calls, and even record remote conversations.  What am I? : **stalkerware**
   1. Searching around, I was thinking **spyware**.
   2. Found, Technology Safety's [Spyware/Stalkerware Overview][5] site to help explain it.
3. ![red-x] This is one of the most common ways a victim of abuse can be compromised digitally.  This can be corrected quite easily and length always trumps complexity.
4. ![check] Boys who witness domestic violence are 2 times as likely to abuse their own partners and children when they become adults.  true or false? : **true**
5. ![check] Professor Chaos was just spotted in a hot tub near a beach.  Someone said he was trying to find safe haven in a country that had a lot of coastlines one that may be the largest lusophone country in the world.  Where oh where could this trickster be?
   1. [A Lusophone is someone who speaks the Portuguese language either as a native speaker, as an additional language, or as a learner.][7]
   2. <br /> ![Google Search][6]

[1]: https://twitter/OSPASafeEscape "@OSPASafeEscape"
[2]: https://www.base64decode.org "Base64Decode"
[3]: https://hexed.it "Hex Editor"
[4]: ../file/0ad3481309685561147dd2b0488d1768.jpg "Photo #1"
[5]: https://www.techsafety.org/spyware "Spyware/Stalkerware Overview"
[6]: images/google-search-q5.png "Google Search"
[7]: https://www.definitions.net/definition/lusophone#:~:text=A%20Lusophone%20is%20someone%20who,most%20of%20modern%2Dday%20Portugal. "Definitions of Lusophone"
[check]: images/checkmark.png "Checkmark"
[x-red]: images/x-red.png "Red X"
[yield]: images/yield.png "Yield"
[question-mark]: images/question-mark.png "Question Mark"
