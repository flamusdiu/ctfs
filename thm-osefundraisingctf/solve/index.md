# OSE Fundraising CTF - July 23 - 24 2021

![1]

## Overview

This is CTF support raising funs for [@OSPASafeEscape]. They raised around $2500 over the two day CTF.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Congratulations to the winners of the first <a href="https://twitter.com/hashtag/safeescapefundraiser?src=hash&amp;ref_src=twsrc%5Etfw">#safeescapefundraiser</a> osint challenge!<br><br>In first place, we have Spikeroche, with 1560 points.<br><br>In second is Kaill with 1040, followed closely by Lotohos with an even 1000.<br><br>Great work, all! Please DM us the arrange for your prizes! <a href="https://t.co/4QsyjACtrb">pic.twitter.com/4QsyjACtrb</a></p>&mdash; Operation: Safe Escape (@OSPASafeEscape) <a href="https://twitter.com/OSPASafeEscape/status/1419339338358398976?ref_src=twsrc%5Etfw">July 25, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

I only found out on day two. I did learn a lot and still have ways to go on my OSINT skills.

For the challenges, I have marked each question with 3 symbols:

<table>
<thead>
<th>Name</th>
<th>Icon</th>
<th>Note</th>
</thead>
<tr>
<td>Checkmark</td>
<td><img src="images/checkmark.png" alt="Checkmark" /></td>
<td>Correctly Answered</td>
</tr>
<tr>
<td>X (red)</td>
<td><img src="images/x-red.png" alt="X (red)" /></td>
<td>Incorrect, no information</td>
</tr>
<tr>
<td>yield</td>
<td><img src="images/yield.png" alt="Yield" /> </td>
<td>Some information is known</td>
</tr>
<tr>
<td>question mark</td>
<td><img src="images/question-mark.png" alt="Question Mark" /></td>
<td>Unsolvable. Missing CTF Files</td>
</tr>
</table>

## Challenges

* [Task 1: CTF #1][2]
* [Task 2: CTF #2][3]
* [Task 3: CTF #3][4]
* [Task 4: CTF #4][5]
* [Task 5: CTF #5][6]
* [Task 6: Super Secret Extra Special Limited Time Shiny OSINT Number Challenge][7]

[1]: images/thm-cft-room-scoreboard.png "Scoreboard"
[2]: task-1.md "Task 1"
[3]: task-2.md "Task 2"
[4]: task-3.md "Task 3"
[5]: task-4.md "Task 4"
[6]: task-5.md "Task 5"
[7]: task-5.md "Task 6"
[@OSPASafeEscape]: https://twitter.com/ospasafeescape
