# Task 2

## Overview

Having issues, found an error, or think something isn't quite right?

Hit us up on Twitter with [@OSPASafeEscape][1]

RULES:

This CTF is designed to be for all skill levels.  Hints may or may not be provided.  All answers must be in lowercase, grammar (commas) and spelling count, and spaces may be applicable as well.

Tools you may or may not need:

Base64 Encoding - [https://www.base64decode.org][2]

HEX Editor - [https://hexed.it][3]

DiskDrill - [https://www.anyrecover.com/data-recovery/?media=googleppc&gclid=CjwKCAjwruSHBhAtEiwA_qCppr5ZtEL4fHqcbqXu76KsMsCfbBrtezK4vwcZEac5Pu7UHR-WOkxzJBoC3S4QAvD_BwE][4]

---

Google Drive link for Question #6 (Photo - ed4042dd46a6974d2f5a26c63ff32dbb.jpg):

[https&colon;&sol;&sol;drive&period;google&period;com&sol;file&sol;d&sol;1HXgaMQ7QCXejUqQc&lowbar;j7icjkp5liVVShv&sol;view&quest;usp&equals;sharing][6]

Google Drive link for Question #7 (Photo - Loki.jpg):

[https&colon;&sol;&sol;drive&period;google&period;com&sol;file&sol;d&sol;1NeCKnF28Yji-XO5aVfhRHxMbiss2zDmK&sol;view&quest;usp&equals;sharingPhoto][7]

## Answer the questions below

1. What is a set of actions that can help lower your risk of being hurt by your partner?
2. ![check] Domestic violence is the leading cause of injury to women - more than car accidents, muggings, and rapes combined. (true or false) : **true**
   1. [National Statistics for Domestic Violence][8]
3. ![check] The gunpowder plot was an effort to destroy Professor Chaos's favorite building ever, what was it? : **No Answer Needed**
4. ![check] What is the national domestic hotline's number? : **1.800.799.7233**
   1. Battered Women's Justice Project website - [The National Domestic Violence Hotline][9]
5. ![check]Professor Chaos was seen fleeing the olympics wearing a flag with a green rectangle on the left of a larger red one.  I think there was a coat of arms on it too.....  Where could they be going? : **Portugal**
   1. View [National Flags by design (Coat of Arms)][11]
   2. <br /> ![Flag of Portugal][10]
6. ![yield] What flag will keep the hostages from chugging cream corn? (Use the Google Drive link above for photo ed4042dd46a6974d2f5a26c63ff32dbb.jpg)
   1. <br /> ![Photo - ed4042dd46a6974d2f5a26c63ff32dbb.jpg][6]
   2. Reverse imaging searching shows this is an edited version of Disney's [Tower of Terror][12].
7. ![yield] What word will get the captives brownies? (Use the Google Drive link above for photo Loki.jpg)
   1. <br /> ![Photo - Loki.jpg][7]
   2. This is meme coming from a [Tiktok Trend][13].

[1]: https://twitter/OSPASafeEscape "@OSPASafeEscape"
[2]: https://www.base64decode.org "Base64Decode"
[3]: https://hexed.it "Hex Editor"
[6]: ../file/ed4042dd46a6974d2f5a26c63ff32dbb.jpg "Photo - ed4042dd46a6974d2f5a26c63ff32dbb.jpg"
[7]: ../file/Loki.jpg "Photo - Loki.jpg"
[8]: https://ncadv.org/statistics "National Statistics"
[9]: https://www.bwjp.org/resource-center/resource-results/the-national-domestic-violence-hotline.html "The National Domestic Violence Hotline"
[10]: images/flag-of-portugal.png "Flag of Portugal"
[11]: https://en.wikipedia.org/wiki/List_of_national_flags_by_design#Coat_of_arms "National Flags by design (Coat of Arms)"
[12]: https://disneyworld.disney.go.com/attractions/hollywood-studios/twilight-zone-tower-of-terror/ "Twilight Zone Tower of Terror™"
[13]: https://tiktokmerch.com/blogs/tiktok-trends/chances-are-low-but-never-zero-killed-trend "'CHANCES ARE LOW... BUT NEVER ZERO' TIKTOK TREND"
[check]: images/checkmark.png "Checkmark"
[x-red]: images/x-red.png "Red X"
[yield]: images/yield.png "Yield"
[question-mark]: images/question-mark.png "Question Mark"