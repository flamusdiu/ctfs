# Task 6

## Overview

**CHALLENGE LEVEL -> HARD** You've been warned  :)

Who should do this challenge:

ANYONE who loves to ride the struggle bus!
A couple notes about this challenge so you don't pull your hair out:

* Information may not be synchronously given to complete this challenge
* If you are having issues, found an error, or think something isn't quite right, please send a message on Twitter to: @OSPASafeEscape

Hit us up on Twitter with [@OSPASafeEscape][1]

RULES:

This CTF is designed to be for all skill levels.  Hints may or may not be provided.  All answers must be in lowercase, grammar (commas) and spelling count, and spaces may be applicable as well.

Tools you may or may not need:

Base64 Encoding - [https://www.base64decode.org][2]

HEX Editor - [https://hexed.it][3]

DiskDrill - [https://www.anyrecover.com/data-recovery/?media=googleppc&gclid=CjwKCAjwruSHBhAtEiwA_qCppr5ZtEL4fHqcbqXu76KsMsCfbBrtezK4vwcZEac5Pu7UHR-WOkxzJBoC3S4QAvD_BwE][4]

---

Google Drive for archive.dmg file, this will be used for this entire OSINT CTF:

[https://drive.google.com/file/d/1BYZVnVoZN0l-f43CFWW3LtLzqJ4wTq6r/view?usp=sharing][4]

## Answer the questions below

1. ![red-x] What was the password to open the archive.dmg file?
2. ![red-x] What was in the archive.dmg file?
3. ![red-x] What was the password for what was found inside the archive.dmg file?
4. ![red-x] What is the password for this file:  Xfinity_install_file?
5. ![red-x] What is the flag inside of the Xfinity_install_file?
6. ![red-x] What type of file was encrypted with a password?
7. ![red-x] What link was found?
8. ![red-x] What number was in the link?

[1]: https://twitter/OSPASafeEscape "@OSPASafeEscape"
[2]: https://www.base64decode.org "Base64Decode"
[3]: https://hexed.it "Hex Editor"
[4]: ../file/archive.dmg "archive.dmg"
