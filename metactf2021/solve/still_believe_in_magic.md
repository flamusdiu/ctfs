# Still Believe in Magic?

## Question

We found [an archive with a file](../file/magic.tar.gz) in it, but there was no file extension so we're not sure what it is. Can you figure out what kind of file it is and then open it?

## Solution

Extracting `magic.tar.gz` gives you a single file `magic`.

You could run `file` on it or check the first bytes:

![010 editor](images/still_believe_in_magic.png)

Rename `magic` to `magic.zip` and extract the `magic.txt` file.

Opening the `magic.txt` file:

![flag](images/still_believe_in_magic_txt.png)

flag: MetaCTF{was_it_a_magic_trick_or_magic_bytes?}
