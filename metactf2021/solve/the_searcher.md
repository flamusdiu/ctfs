# The Searcher

## Question

Alright analyst. We need your help with some investigative work as we dive deeper into one of the infections on our company's network. We've taken [a small packet capture](../file/the_searcher.pcapng) that we know contains some C2 traffic. In order to give us some more leads for the investigation though, we'd like to see if we can identify what C2 framework the attacker was using. This will give us some leads into potential host-based artifacts that might be left behind.

Please submit the name of the C2 Framework being used in the form of MetaCTF{c2frameworkname}

## Solution

1. Examining the PCAP file:

    ![wireshark](images/the_searcher_wireshark.png)

    You'll notice several GET/POSTs in the HTTP stream:

    * /en-us/test.html
    * /en-us/docs.html
    * /en-us/index.html

    These **should** stand out because they are strange. Also, documentation for web server software should be removed from the web server in most cases.

2. Performing a web search using `malware "en-us/docs.html"` to see what shows up.

    ![search](images/the_searcher_search.png)

    I checked (1) but it did not yield any information about the malware but showed the IOCs for it. Then, way at the bottom, (2) explain about the Convenant C2 Infrastructure. Reading the page yields the same GET/POST requests examined in the PCAP file.

flag: covenant
