# There Are No Strings on Me

## Question

We've got this program that's supposed to check a password, and we're not quite sure how it works. Could you take a look at it and see about finding the password it's looking for?

## Solution

![010_editor](images/there_are_no_strings.png)

You could just run "strings" from a Linux terminal instead of using 010 Editor.
