# Sugar, We're Goin Up

## Question

In September 2021, GitLab upgraded the CVSSv3 score for a critical remote code execution vulnerability to 10.0, the highest possible score. Although a patch was released in April, numerous public-facing, unpatched GitLab instances remain vulnerable.

What is the CVE number for this critical, actively exploited vulnerability? The flag format will be CVE-XXXX-XXXX.

## Solution

Using a search engine: gitlab + CVSSv3

Website: [GitLab Unauthenticated Remote Code Execution CVE-2021-22205 Exploited in the Wild](https://www.rapid7.com/blog/post/2021/11/01/gitlab-unauthenticated-remote-code-execution-cve-2021-22205-exploited-in-the-wild/)

Flag: CVE-2021-22205
