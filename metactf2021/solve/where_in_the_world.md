# Where in the World?

## Question

I must say, every time I see one of these directional signs, I think I've got to make this into a CTF problem. It's the idea of Open Source Intelligence (OSINT) or Geospatial Intelligence (GEOINT). The idea of being able to take an image and use all of the clues within it to infer details such as where it's at or what's happening in the photo.

Here is one such picture of those signs. Your goal? Use those little details to find the name of the marina it's at which you'll submit for the flag (so MetaCTF{name of marina}

## Solution

1. Examining the image:

    ![image](images/where_in_the_world.png)

    You'll notice boat sails and several unique places not far from the sign. This probably near water due to the size of the boat sails in the image.

2. If you search up "Gill Rock", you'll find it here:

    ![google maps](images/where_in_the_world_maps1.png)

    Also, look at the direction of Milwaukee and Chicago -- to the south of Gill Rock.

    ![image](images/where_in_the_world2.png)

    This point north (assuming we are south closer to Milwaukee here).

    ![google maps](images/where_in_the_world_maps2.png)

    Search around you, will see that Egg Harbor is about 19 m. If you look at images there:

    ![egg harbor](images/where_in_the_world_egg_harbor.png)

    Several parts of this sign matches the sign we ware looking for.

flag: egg harbor