# A to Z

## Question

This encrypted flag will only require a simple substitution cipher to solve. Rearrange the letters from A to Z.

yzhsufo_rh_nb_uze_wdziu

## Solution

This is a single substitute cipher. You can solve it by the following script or use [CyberChef](https://gchq.github.io/CyberChef/#recipe=Atbash_Cipher()&input=eXpoc3Vmb19yaF9uYl91emVfd2R6aXU)

```python
alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
cypher = "yzhsufo_rh_nb_uze_wdziu"
alpha_rev = alpha[::-1].lower()

plain = ""
for letter in cypher:
    loc = alpha_rev.find(letter)

    if loc >= 0:
        plain += alpha[loc]
    else:
        plain += "_"
        
print(plain)
```

flag: bashful_is_my_fav_dwarf
