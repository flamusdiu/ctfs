# Easy as it (TCP) Streams

## Question

Caleb was designing a problem for MetaCTF where the flag would be in the telnet plaintext. Unfortunately, he accidentally stopped the [packet capture](../file/easy_as_it_streams.pcapng) right before the flag was supposed to be revealed. Can you still find the flag? Note: You'll need to decrypt in CyberChef rather than using a command line utility.

## Solution

Right click first packet, Follow > TCP Stream:

![stream 1](images/easy_as_it_streams_wireshark1.png)

This gives you the PGP message. Remember packet #8 (last packet in this stream).

Move past packet #8 to the next TCP packet (#11):

![stream 2](images/easy_as_it_streams_wireshark2.png)

Now, you have the Private key. Remember packet #18 (last packet in this stream).

Move past packet #18 to the next TCP packet (#25):

![stream 3](images/easy_as_it_streams_wireshark3.png)

Make sure you change the conversation from "Entire conversation" to the one right below it.

Now, you also have the private key password.

Next, use [Cyberchef](https://gchq.github.io/CyberChef/):

![cyberchef](images/easy_as_it_streams_cyberchef1.png)

Click the "Magic wand" to get the flag.

flag: MetaCTF{cleartext_private_pgp_keys}
