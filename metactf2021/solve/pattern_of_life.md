# Pattern of Life

## Question

Hackers have breached our network. We know they are there, hiding in the shadows as users continue to browse the web like normal. As a threat hunter, your job is to constantly be searching our environment for any signs of malicious behavior.

Today you just received [a packet capture (pcap)](../file/pattern_of_life.pcapng) from a user's workstation. We think that an attacker may have compromised the user's machine and that the computer is beaconing out to their command and control (C2) server. Based on some other logs, we also think the attacker was *not* using a fully encrypted protocol and also did not put much care into making their C2 server look like a normal website. Your task? We'd like you to submit the port number that the C2 server is listening on in the form of MetaCTF{portnumber} as the flag.

## Solution

Using the display filter: `tcp.port!=443&&tcp.port!=80`:

![wireshark](images/pattern_of_life.png)

You will notice the packets alternate perform an HTTP request (this view is sorted by port).

Scroll down until you get `52.144.115.131` as a Destination.

flag: MetaCTF{8080}
