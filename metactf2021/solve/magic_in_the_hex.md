# Magic in the Hex

## Question

Sometimes in forensics, we run into files that have odd or unknown file extensions. In these cases, it's helpful to look at some of the file format signatures to figure out what they are. We use something called "magic bytes" which are the first few bytes of a file.

What is the ASCII representation of the magic bytes for a VMDK file? The flag format will be 3-4 letters (there are two correct answers).

## Solution

Website of magic numbers: [GCK's File Signatures Table](https://www.garykessler.net/library/file_sigs.html)

Either of these:

![file_sigs](images/magic_in_hex.png)

flag: kdm OR kdmv
