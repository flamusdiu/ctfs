# Flag Format

## Question

Most of our flags are formatted like this: `MetaCTF{string_separated_with_und3rscores}`

If the flag is not in that format, we specify what the flag format should be instead. If you solve this challenge, make sure to tell your teammates about the flag format!

## Solve

flag: MetaCTF{string_separated_with_und3rscores}
