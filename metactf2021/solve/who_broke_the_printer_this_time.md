# Who Broke the Printer This Time?

## Question

Malicious operators typically exploit unpatched vulnerabilities within target environments to gain initial access, escalate privileges, and more.

What recent vulnerability have Conti ransomware operators exploited to run arbitrary code with SYSTEM privileges?

The flag format will be CVE-xxxx-xxxxx

## Solution

Website: [Windows Print Spooler Remote Code Execution Vulnerability](https://msrc.microsoft.com/update-guide/vulnerability/CVE-2021-34527)

flag: CVE-2021-34527
