# I Just Wanna Run

## Question

Our security team has identified evidence of ransomware deployment staging in the network. We’re trying to contain and remediate the malicious operator’s deployment staging and access before the operator successfully spreads and executes ransomware within the environment. We’ve recovered some of the operator’s [staging scripts and files](../file/incident017.zip). Can you help identify which user account’s credentials the operator had compromised and is planning to use to execute the ransomware?

The flag format will be METAL\xxxxx

## Solution

The **incident017.zip** contains the following files:

![zip file](images/incident017.png)

Opening the `exe.bat` file gives the following contents:

![exe.bat](images/incident017_exe_bat.png)

flag: METAL\timq-admin
