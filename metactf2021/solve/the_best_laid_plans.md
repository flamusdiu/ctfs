# The Best Laid Plans...

## Question

Sometimes, routers can break packets up into fragments to meet abnormal networking requirements, and the endpoint will be responsible for putting these back together. Sometimes however, this doesn't go as planned, as Microsoft found out with CVE-2021-24074. We'd like to see the function responsible for this vulnerability, but we're having some trouble finding its name... Could you see if you could find it?

## Solution

After searching around, I ran into this blog post:

Website: [Researchers Follow the Breadcrumbs: The Latest Vulnerabilities in Windows’ Network Stack](https://www.mcafee.com/blogs/other-blogs/mcafee-labs/researchers-follow-the-breadcrumbs-the-latest-vulnerabilities-in-windows-network-stack/)

![McAfee Blog](images/the_best_laid_plans.png)
