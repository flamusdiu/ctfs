# Et tu, Hacker?

## Question

The law firm of William, Ian, Laura, and Lenny (WILL for short) has just been the victim of an attempted cyber attack. Someone tried to brute force the login for one of their employees. They have the [event logs](../file/bruteforce.evtx) of the incident, and were wondering if you could tell them which user was targeted. Flag is in the form of MetaCTF{}.

## Solution

![event viewer](images/et_tu_hacker.png)

Several logon events (4625, 4648) contain the login failure for a single user: `ericm`.

flag: ericm
