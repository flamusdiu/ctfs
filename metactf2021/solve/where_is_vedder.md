# Where's Vedder?

## Question

Please help find our dear friend Vedder Casyn. His last known location was [this location](../file/location.jpg). We believe it's within a public forest area in his home state.

The answer should represent the MD5 hash of the address of the location. For example, if the address is: "150 Greenwich St, New York, NY 10006" then the flag will be e8244cb2f4d53117e9797af909123e86. Make sure to have the address format the same as above.

[This video](https://www.youtube.com/watch?v=RoqWbpZUOSo) is helpful for thinking about the right way.

## Solution

![image](images/where_vedder.png)

1. Perform internet search for "Vedder Casyn":

    ![search](images/where_vedder_search.png)

    Shows all his social media accounts.

2. Going to his twitter page:

    ![twitter](images/where_vedder_twitter.png)

    Notice "Hammond, Indiana"

3. Go to [Google Maps](https://maps.google.com) and search for "Indiana state forest". All the forest have "state" with one as a "national" forest.

    ![google amps](images/where_vedder_google_maps.png)

    Going to check Hoosier National Forest.

4. Clear the search, and search for "funeral home" (remember the cars in the original image looked to be funeral cars):

    ![google maps](images/where_vedder_maps2.png)

    (1) shows the forest. (2) marks the closes funeral home. Checking that gives the following:

    ![google maps](images/where_vedder_google_maps3.png)

    The building in the image is the same based on the same markers from the original image.

5. Take the address "628 IN-64, English, IN 47118" and use [CyberChef](https://gchq.github.io/CyberChef):

    ![cyber chef](images/where_vedder_cyberchef.png)

flag: 7be0798af71f79eadb9254d3554aa301
