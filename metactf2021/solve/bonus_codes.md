# Bonus Codes

## From Live Stream

* ​can't_talk_right_now?_im_busy_watching_a_livestream
* an_apple_a_day_does_not_keep_the_attackers_away
* is_this_thing_on?_can_you_hear_me?
* ​compete_hack_learn_repeat
* for_listening
* This at the top of the slides being shown from MetaCTF:
  ![morse_code](images/morse_code_video.png)
  
  ```txt
  .- .-.. .-- .- -.-- ... ..--.- -.- . . .--. ..--.- .- -.
  ..--.- . -.-- . ..--.- ..- - ..--.- -. . ...- . .-. ..--.-
  -.- -. --- .-- ..--.- .-- .... .- - ..--.- -.-- --- ..-
  .----. .-.. .-.. ..--.- ..-. .. -. -..
  ```
  
  Use [Cyberchef](https://gchq.github.io/CyberChef/#recipe=From_Morse_Code('Space','Line%20feed')&input=Li0gLi0uLiAuLS0gLi0gLS4tLSAuLi4gLi4tLS4tIC0uLSAuIC4gLi0tLiAuLi0tLi0gLi0gLS4gLi4tLS4tIC4gLS4tLSAuIC4uLS0uLSAuLi0gLSAuLi0tLi0gLS4gLiAuLi4tIC4gLi0uIC4uLS0uLSAtLi0gLS4gLS0tIC4tLSAuLi0tLi0gLi0tIC4uLi4gLi0gLSAuLi0tLi0gLS4tLSAtLS0gLi4tIC4tLS0tLiAuLS4uIC4tLi4gLi4tLS4tIC4uLS4gLi4gLS4gLS4u)

  flag: ALWAYS_KEEP_AN_EYE_UT_NEVER_KNOW_WHAT_YOU'LL_FIND

* See: <https://youtu.be/EG_S4DXCFCU?t=12593>

  It is reversed in the video, use something to reverse the image if you cannot read it.

  ![bonus_wall](images/bonus_wall.png)
