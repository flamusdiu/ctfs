# Challenges

![scoreboard](images/scoreboard.png)

![scoreboard](images/scoreboared2.png)

[**Bonus Flags**](bonus_codes.md)

## Binary Exploitation

## Cryptography

* [Q#2107 A to Z](a_to_z.md)
* [Q#4004 Thnks fr th Pwds](thnk_fr_th_pwds.md)
* [Q#4008 Wrong Way on a One Way Street](wrong_way_on_a_one_way_street.md)
* [Q#4009 Unbreakable Encryption](unbreakble_encryption.md)
* [Q# Size Matters](size_matters.md)

## Forensics

* [Q#4002 Magic in the Hex](magic_in_the_hex.md)
* [Q#4005 My Logs Know What You Did](my_logs_know_what_you_did.md)
* [Q#4027 I Just Wanna Run](i_just_wanna_run.md)
* [Q#4051 Sharing Files and Passwords](sharing_files_and_passwords.md)
* [Q#4003 Still Believe in Magic?](still_believe_in_magic.md)
* [Q#4018 Et tu, Hacker?](et_tu_hacker.md)
* [Q#2105 Easy as it (TCP) Streams](easy_as_it_streams.md)
* [Q#4019 Pattern of Life](pattern_of_life.md)

## Reconnaissance

* [Q#4007 Sugar, We're Goin Up](sugar_were_goin_up.md)
* [Q#4012 The Best Laid Plans...](the_best_laid_plans.md)
* [Q#4028 Who Broke the Printer This Time?](who_broke_the_printer_this_time.md)
* [Q#2019 Who owns the cloud?](who_owns_the_cloud.md)
* [Q#4020 The Searcher](the_searcher.md)
* [Q#4017 Where in the World?](where_in_the_world.md)
* [Q#2108 Mystery C2 Server](mystery_c2_server.md)
* [Q#4031 Where's Vedder](where_is_vedder.md)

## Reverse Engineering

* [Q#4013 There are No Strings on Me](there_are_no_strings.md)

## Web Exploitation

* [Q#4029 Under Inspection](under_inspection.md)

## Other

* [Q#4001 Flag Format](flag_format.md)
* [Q#4046 Interception I](interception_i.md)
* [Q#4006 This Ain't a Scene, It's an Encryption Race](this_aint_a_scene.md)
* [Q#4048 Interception III](interception_iii.md)
