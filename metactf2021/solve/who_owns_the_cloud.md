# Who owns the cloud?

## Question

In conducting Open Source Intelligence (OSINT), we act as sort of "cyber detectives," finding little tidbits of information and connecting them to put together a more complete understanding of something.

Take [this datacenter](https://goo.gl/maps/jacryehbW6NCgD9d9) for example. Usually it is very easy to figure out what company owns a building - especially when it's a giant facility and the company puts their logo on it. But what if the obvious visual clues aren't there? Dig deeper and see if you can find the company that *directly* owns this piece of land. You'll want to submit the name of the company as the flag.

## Solution

1. Check [Google Maps](https://www.google.com/maps/place/45261+W+Severn+Way,+Sterling,+VA+20166/@39.0208243,-77.4355891,3a,60y,133.61h,91.51t/data=!3m6!1e1!3m4!1sfrHO3EMrhH03u6xW3oCOjg!2e0!7i13312!8i6656!4m13!1m7!3m6!1s0x89b6391acb68888f:0xa3256a6ad7d9dbcc!2s45261+W+Severn+Way,+Sterling,+VA+20166!3b1!8m2!3d39.0208873!4d-77.4355454!3m4!1s0x89b6391acb68888f:0xa3256a6ad7d9dbcc!8m2!3d39.0208873!4d-77.4355454) for that location. You will see the following building:

    ![google maps](images/who_owns_the_cloud_google_maps.png)

    The marked location are unique. I did attempt to reverse image search but that did not yield anything. So, I went to normal searching.

2. Using [DuckDuckGo](https://duckduckgo.com), search for `datacenter sterling, va`)

    ![duckduckgo](images/who_owns_the_cloud_search1.png)

3. Going to the [Sterling Data Center Market](https://www.datacenters.com/locations/virginia/sterling), scroll down a bit.

    ![datacenter market](images/who_owns_the_cloud_market.png)

    This looks similar to the image on Google Maps (see #1 above).

    ![datacenter market image](images/who_owns_the_cloud_market2.png)

    The same points match in both images.

4. The datacenter is owned by Amazon AWS:

    ![datacenter company info](images/who_owns_the_cloud_market3.png)

    However, this does not work. Amazon runs the datacenter but does not own the land. That requires property search.

5. Doing a quick search lands you on Loudoun County VA [property search page](https://reparcelasmt.loudoun.gov/pt/search/CommonSearch.aspx?mode=REALPROP#).

    ![property search](images/who_owns_the_cloud_property_search.png)

    From here, perform a search using location 1 and 2 and hitting "Search". Then, choose the check box at the last proper (which is the closes to the one we need), and choose location 5 to view the map.

    ![property map](images/who_owns_the_cloud_property%20map.png)

    (1) gets me across the street from the proper location. Choose the other building g(at (2)) gives me property information for the owner.

flag: BCORE COPT DC-21 LLC
