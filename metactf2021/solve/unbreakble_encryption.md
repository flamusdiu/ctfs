# Unbreakable Encryption

## Question

There is a form of truly unbreakable encryption: the one time pad. Nobody, not Russia, not China, and not even Steve, who lives in his mom's basement and hacks governments for fun, can decrypt anything using this cipher... as long as it's used correctly. In this scheme, a truly random string as long as the plaintext is chosen, and the ciphertext is computed as the bitwise XOR of the plaintext and the key. However, if the key is reused even once, it can be cracked. We've intercepted some messages between some criminals, and we're hoping you could crack the one time pad they used. We're pretty sure they reused it, so you should be able to crack it...

Ciphertext 1: `4fd098298db95b7f1bc205b0a6d8ac15f1f821d72fbfa979d1c2148a24feaafdee8d3108e8ce29c3ce1291`

Plaintext 1: `hey let's rob the bank at midnight tonight!`

Ciphertext 2: `41d9806ec1b55c78258703be87ac9e06edb7369133b1d67ac0960d8632cfb7f2e7974e0ff3c536c1871b`

## Solution

Website: [dcode XOR Cipher](https://www.dcode.fr/xor-cipher#q2)

![decode XOR Cipher](images/unbreakable_encryption_dcode.png)

Take the results:

```txt
27 B5 E1 09 E1 DC 2F 58 68 E2 77 DF C4 F8 D8 7D 94 D8 43 B6 41 D4 89 18 A5 E2 79 E3 40 90 C3 9A 86 F9 11 7C 87 A0 40 A4 A6 66 B0
```

Remove the spaces:

```txt
27B5E109E1DC2F5868E277DFC4F8D87D94D843B641D48918A5E279E34090C39A86F9117C87A040A4A666B0
```

Check the key using [Cyberchef](https://gchq.github.io/CyberChef):

![cyberchef](images/unbreakable_encryption_cyberchef_1.png)

Here the key gives us the plain text.

Replace the cypher text:

![cyberchef](images/unbreakable_encryption_cyberchef_2.png)

flag: MetaCTF{you're_better_than_steve!}
