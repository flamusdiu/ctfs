# Interception III

## Question

192.168.55.3 is periodically sending the flag to 172.16.0.2 over UDP port 8000. Go get it.
By the way, I've been told the admins at this organization use really shoddy passwords.

```bash
ssh ctf-f36ef72cadc1@host.cg21.metaproblems.com -p 7000
```

**Note**: The password for this user is the flag from Interception I. You must finish Interception I before starting this challenge.

Hint:
    ![network map](images/intercept_iii_network_map.png)

## Solution

1. First, if you port scan `192.168.0.1`, you'll find it has only one port open--23 (telnet). Connect to that port using `telnet` (not `nc`, or this will give you a blank screen with zero data in it).

    ![router login](images/intercept_III_router_login.png)

2. If you look in the user folder, you'll see a folder for [bird](http://bird.network.cz):

    ![bird](images/intercept_iii_bird.png)

    So, bird acts like router software. Okay, now, how do we interact with the software?

3. Take a look at Bird doc for [Remote Control](https://bird.network.cz/?get_doc&v=16&f=bird-4.html). This provides a way using `birdc` command.

4. Next, there is already `tcpdump` on the computer. So, we need to reroute traffic through our router. At the moment:

    ```txt
    [client] <-> ex <-> it <-> <server>
    ```

    What we need is:

    ```txt
    [client] <-> ex <-> sa <-> it <-> <server>
    ```

    This will allow us to capture the traffic. So, do this, we need to change the routing metrics for OSPF for the `172.16.0.0/16` prefix.

5. Bird docs on [OSPF](https://bird.network.cz/?get_doc&v=16&f=bird-6.html#ss6.8) shows a JSON-ish configure file. This is located at `/usr/local/etc/bird.conf`.

    Pulling this into vi by `vi /usr/local/etc/bird.conf`. Make one small edit on line 38:

    ![bird.conf](images/intercept_iii_bird_conf.png)

    Then, exit `vi` and run `birdc` and issue the command `configure`.

    ![birdc reconfigure](images/intercept_iii_bird2.png)

6. Exit out of `birdc` and run `tcpdump -i enp1s0 -vv -X dst 172.16.0.2`

    ![tcpdump](images/intercept_iii_tcpdump.png)

flag: MetaCTF{l00k_at_m3_1m_th3_r0ut3r_n0w}
