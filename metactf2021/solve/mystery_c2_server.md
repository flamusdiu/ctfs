# Mystery C2 Server

## Question

There is a C2 server running an HTTPS listener at 50.17.89.130:1337 can you determine what C2 framework it is? The flag is the name of the C2 framework.
Example: MetaCTF{Cobalt Strike}

Careful--you only have 10 attempts for this one.

## Solution

1. Finger print the server:

    ![nmap](images/mystery_c2_nmap.png)

    Port shows: `ssl/http Golang net/http server (Go-IPFS json-rcp or InfluxDB API)`

2. Check the [C2 Matrix](https://docs.google.com/spreadsheets/d/1b4mUxa6cDQuTV2BPC6aA-GR4zGZi0ooPYtBe4IgPsSc/edit#gid=0)

    ![google doc](images/mystery_c2_c2matrix.png)

3. Of the two, `DeimosC2` is the only OSS.

flag: deimosc2
