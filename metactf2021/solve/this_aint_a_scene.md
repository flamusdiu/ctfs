# This Ain't a Scene, It's an Encryption Race

## Question

Ransomware attacks continue to negatively impact businesses around the world. What is the Mitre ATT&CK technique ID for the encryption of data in an environment to disrupt business operations?

The flag format will be T####.

## Solution

Website: [MITRE ATT&CK](https://attack.mitre.org/)

Specifically: [T1486](https://attack.mitre.org/techniques/T1486/)
