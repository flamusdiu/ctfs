# Sharing Files and Passwords

## Question

FTP servers are made to share files, but if its communications are not encrypted, it might be sharing passwords as well. The password in this pcap to get the flag

## Solution

![wireshark](images/sharing_files_and_passwords.png)

flag: ftp_is_better_than_dropbox
