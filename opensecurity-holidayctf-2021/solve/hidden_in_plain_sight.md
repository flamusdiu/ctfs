# Hidden In Plain Sight

## Question

Something is hidden here, I just know it. But where?! There doesn't seem to be anything here. Except for that darn bird!

## Hint

The bird seems a lil sus, I have reason to believe that that is 100% not a love bird. In fact I have suspicions it may be far more malicious. Perhaps a spy from the South Pole. Intelligence says these birds have been used to send encoded messages. Maybe try some common stegonography techniques.

## Solution

The page opens with:

![start](images/hidden_in_plain_sight_start.png)

The page is blurry and their are words. Looking at the source code of the page:

![source code](images/hidden_in_plain_sight_source.png)

Well, that's not very nice.

If you look a above this, you'll see:

![source code](images/hidden_in_plain_sight_source2.png)

This reveals the url [https://ctf.opensecurity.io/lovebirds.jpg](file/lovebirds.jpg):

![lovebirds](images/hidden_in_plain_sight_lovebirds.png)

This file is nearly 22M. This is a good indication that something is hidden within the file.

The first tool to try is [steghide](http://steghide.sourceforge.net/).

    $ steghide extract -sf lovebirds.jpg -p ""
    wrote extracted data to "sketch_bird.jpg".

Yes, this is a "blank" password. It wrote out another file. Run the command against the new file.

    $steghide extract -sf sketch_bird.jpg -p ""
    wrote extracted data to "message.txt".

Examine the new file `message.txt`:

    $cat message.txt 
    SQUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACK
    OS{BIRB_NOIS3S}

flag: OS{BIRB_NOIS3S}
