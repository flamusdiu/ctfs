# Challenges

    Open Security Holiday CTF Instructions
    🏆 Prizes:
    🢒 The first five finalists will be chosen to win a high-quality t-shirt.

    📆   When: 
    🢒 Monday December 6th, 12PM CST - Friday, December 17th, 12PM CST.

    🚥   Status: 
    🢒 ONLINE: 🟢 

    ℹ️  Registration: 
    🢒 Once we're live, type /register in this channel.
    🢒 Then the bot will give you a secret token that you can use to register with the CTF Platform.

    🚩 All flags are formatted like os{...} or OS{...}

    ⚠️ Something broke?
    🢒 Did your session die, or swap to a new device? Reregister with /register to get a new access token. 
    🢒 Other issues? Message @Payton  | Open Security directly. 

This was posted in the Discord after I completed the last challenge:

![Winner](os_discord_winner.png)

This popped up on the CTF site:

![Winner](images/winner2.png)

* [Escaping the Naughty List](escaping_the_naughty_list.md)
* [Gatekeepers](gatekeepers.md)
* [Baking Cookies](baking_cookies.md)
* [Elf Encryption - 8 Layers](elf_encryption_8_layers.md)
* [Hidden in Plain Sight](hidden_in_plain_sight.md)
* [Lost in the Snow](lost_in_the_snow.md)
