# Gatekeepers

## Question

This authentication portal seems extremely vulnerable. Maybe you can find a way to get past the login.

## Hint

The web server is being very talkative, but the backend refuses to listen.

## Solution

The login page:

![login page](images/gatekeepers_login.png)

Clicking on the Login button gives:

![login error](images/gatekeepers_login_error.png)

Examing the source, if you look near the bottom, you will see some javascript:

```javascript
function authorize(){
        location.href = '/welcome.html';
}
    
document.addEventListener("DOMContentLoaded", function(event) {
    $(".alert").hide();

    $("button.submit").click(function(){
        if($(".pass").val().length < 8 ){
            // input is too short.
            alert("E_PASSWORD_TOO_SHORT")
        }
        if($(".pass").val().length > 128){
            // input is too long.
            alert("E_PASSWORD_TOO_LONG")
        }

        $.ajax({url: "/login", data:JSON.stringify({user: $(".user").value, pass: $(".pass").value}),
            success: function(result){
                if(result.includes("ACCESS_GRANTED")){
                    authorize();
                }
            },
            error: function(result){
                $(".alert").fadeOut().fadeIn();
            }});
    })
})
```

Here, an [AJAX](https://api.jquery.com/jquery.ajax/) call where if you have successful login from the `/login` page, then the function `authorize()` runs. However, `authorize()` sends you to `https://ctf.opensecurity.io/welcome.html`. 

Heading over there gives you a "blank" page with the hidden flag:

![welcome](images/gatekeepers_welcome.png)

flag: os{welcome_t0_th4_g@t3s}