# Elf Encryption - 8 Layers

## Question

The elves use a custom crypter to encrypt their zip files. Intelligence indicates that the cipher can't be bruteforced before Christmas, and we're on a real deadline. Would it be possible to break the encryption. Maybe there's a technique we haven't considered.

## Hint

Break out of the Zipcrypt! Maybe this blog will be of interest to you <https://blog.devolutions.net/2020/08/why-you-should-never-use-zipcrypto/>

## Solution

The opening screen:

![start](images/elf_encryption_start.png)

1) Click the **Download** button to get the [Zip file](../file/Elf_Elf_Elf_Elf.zip).
2) Ignore the Password Generator. It does not work. You might find it fun to play with.

From the blog:

> One of the .zip password protection algorithms is called ZipCrypto. ZipCrypto is supported natively on Windows, but it should never be used because it is completely broken, flawed, and relatively easy to crack. All hackers need to know is 12 bytes of plain text and where it is located in the zip (which can be easily found) in order to quickly decrypt the entire content of the archive. To give you an idea, on most laptops, it would usually takes less than a minute to decrypt the entire content of a zip file.

So we know it's broken, we just need to use the information to break it.

From the blog, download

    * [bkcrack](https://github.com/kimci86/bkcrack)
    * [plain.zip](https://webdevolutions.blob.core.windows.net/blog/2020/08/plain.zip)

You need the **plain.zip** to make it easier to break the challenge file. On a Windows machine, put `plain.zip` and `Elf_Elf_Elf_Elf.zip` in the same folder.

> **Note:** I am using [Parrot OS Security](parrotsec.org/security-edition/) and Windows Pro

First, if you examine `Elf_Elf_Elf_Elf.zip` in Windows, and you will see two files:

* decrypt_me.html
* elf.template.reactive-vue.html

We need to decrypt the `decrypt_me.html`. Since the `decyrpt_me.html` is a HTML file we can try to decrypt it using the header from the other file.

You will need to extract `plain.txt` from `plain.zip` and remplace the text with the first line from `elf.template.reactive-vue.html`.

The file will look like:

    <!DOCTYPE HTML PUBLIC>

Then, copy it back to the `plain.zip` replacing `plain.txt`. The reasoning for do this is the ZIP file must have certain attributes on it to match it with encrypted one. From the blog:

> Do not use a password and **use the same compression algorithm as the encrypted archive**.

I had issues with trying to create a ZIP, so I just used the one from the blog which apparently will work.

Now in Parrot OS, you need to make sure that `bkcrack`, `plain.zip` and `Elf_Elf_Elf_Elf.zip` are in the same folder.

Then run `bkcrack`:

    $./bkcrack -C Elf_Elf_Elf_Elf.zip -c decrypt_me.html -P plain.zip -p plain.txt
    bkcrack 1.3.3 - 2021-11-08
    [03:02:34] Z reduction using 14 bytes of known plaintext
    100.0 % (14 / 14)
    [03:02:35] Attack on 488797 Z values at index 7
    Keys: 9fc341f9 add289e3 d5b2f312
    33.5 % (163681 / 488797)
    [03:08:05] Keys
    9fc341f9 add289e3 d5b2f312

If you get the keys, move on to the next command:

    $./bkcrack -C Elf_Elf_Elf_Elf.zip -c decrypt_me.html -k 9fc341f9 add289e3 d5b2f312 -d decrypted_me.html
    bkcrack 1.3.3 - 2021-11-08
    [03:10:44] Writing deciphered data decrypted_me.html (maybe compressed)
    Wrote deciphered data.

Open in a browser:

    $firefox decrypted_me.html 

![final](images/elf_encryption_final.png)

flag: os{congr@tulat10ns_0n_solving_dat!}
