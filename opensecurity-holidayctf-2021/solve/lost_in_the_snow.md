# Lost in the Snow

## Question

The North Pole Weather Service has issued a snowstorm warning for the following regions: North, East, South, and last but not least Caneville. Elves should be advised---BREAKING NEWS: Santa has lost his sled keys. With the current silicon shortage, creation of a new key in time for Christmas is an impossible feat. We need EVERY ELF searching for the sled keys effective immediately. Thank you for your coorporation.

## Hint

If only I had a way to decode the whitespace, maybe I could retrieve the key from the white snow. There's so much it all runs together.

## Solution

If you scroll down the page, you will see:

![start](images/lost_in_the_snow_start.png)

If you right click the frame and choose "View Source". Then, hit `CTRL + A` in the now window.

![frame](images/lost_in_the_snow_frame.png)

Here, you can see there is white space within the frame.

If you use a search engine for search for "decode whitespace", you will land on [Whitespace Language](https://www.dcode.fr/whitespace-language).

You can copy the frame data into the "WHITESPACE CODED CIPHERTEXT" text box. 

> Make sure you **delete** the data in the box first!

Click **Decrypt**. You get the following output under **Results**:

    ...._____________....
    .../......_......\..
    ...[].::.(_).::.[]\
    ...[].:::::::::.[].\_______________
    ...[].:::::::::.[]..\.The.Sled.Keys\
    ...[].:::::::::.[]...\______________\
    ...[].:::::::::.[]
    ...[_____________]
    .......I.....I..
    .......I_..._I..
    ......../...\...
    ........\.../...
    ........(...)...
    ......../...\...
    ........\___/...
    ................
    Oh.my.goodness,.you.found.them.os{you_r3ally_s2ved_the_holid2ys}

flag: os{you_r3ally_s2ved_the_holid2ys}
