// CHECK TOKEN FOR ACCESS API ENDPOINT
app.get('/api/check', (req, res) => {
    user = doauth(req)

    if (!user) {
        userId = (new Date).getTime().toString() + Math.floor(Math.random() * 1000)
        user = {
            userid: userId,
            password: Math.floor(Math.random() * 10 ** 15).toString(),
            firstName: randomValue(firstName),
            lastName: randomValue(lastName),
        }

        jwt.sign(user, secret, (err, token) => {
            if (err) res.json({ status: "Error", log: "Unrecoverable error." })
            res.cookie('token', token)
            res.json({ status: "CreateData", state: "Success" })
        })
    } else {
        //validationChecks();

        if (user.userid > 0) {
            MD5Hash = (new MD5().update((user.userid).toString())).digest('hex').substr(0, 4)
            hashValid = (Number(MD5Hash).toString().length > 8)
            // Privileged user has access to the system.
            if (hashValid) {
                payload = `ssh internal.workshop.tld
                            220 vsSSHd 3.0.2+ (ext,1) ready...
                            User (internal.workshop.tld:(none)): <b>${user.firstName}.${user.lastName}</b> with <b>ADMIN APPROVAL.</b>
                            331 Please specify the password.
                            Password: <b>${user.password}</b> 
                            230 OK. Current restricted directory is /.
                            230 OK. MOTD os{${flag}} 
                            >> [no input]
                            Connection closed by host.
                `
                res.json({ status: "ReadData", data: user, log: payload })
            }else{
                payload = `ssh internal.workshop.tld
                            220 vsSSHd 3.0.2+ (ext,1) ready...
                            User (internal.workshop.tld:(none)): <b>${user.firstName}.${user.lastName}</b>
                            331 Please specify the password.
                            Password: <b>${user.password}</b>
                            230 OK. Current restricted directory is /chroot/jail.
                            !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                            !! USER IS NOT AUTHORIZED FOR REMOTE LOGIN !! 
                            !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                            exit.
                `
                res.json({ status: "ReadData", data: user, log: payload })
            }
        }
    }
});
        
//Verify Token
function doauth(req) {
    let user = Buffer.from(req.cookies["token"], "base64");
    user = user.toString("utf8")
    user = user.substr(user.indexOf("}") + 1)
    user = JSON.parse(user.substr(0, user.indexOf(',"iat"')) + "}")
    return user;
}

//Express Server
app.listen(5001, err => {
    if (err) {
        console.log(err);
    }
    console.log('Server Started on PORT 5000')
})