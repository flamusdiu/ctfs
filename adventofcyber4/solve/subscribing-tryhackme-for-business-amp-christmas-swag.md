# Task#4 - Subscribing, TryHackMe for Business & Christmas Swag!

SubscribingThe Advent of Cyber event is completely free! However, check out some of the reasons to subscribe:

[![why-subscribe](/tryhackme.com/adventofcyber4/file/why-subscribe.png)](https://tryhackme.com/why-subscribe)

In celebration of Advent of Cyber, you can get 20% off personal annual subscriptions, using the discount code `AOC2022` at checkout. This discount is only valid until the 7th of December, that's in: ![counter](https://gen.sendtric.com/countdown/07vkwgax6u)

If you want to gift a TryHackMe VIP subscription, you can [purchase vouchers](https://tryhackme.com/subscriptions).

## Christmas Swag

Want to rep swag from your favorite cyber security training platform?  
We have a special edition Christmas Advent of Cyber t-shirt [available now](https://store.tryhackme.com/collections/all/products/advent-of-cyber-2022-limited-edition) - check our [swag store](https://store.tryhackme.com/) to order yours!

![shirt](/tryhackme.com/adventofcyber4/file/dda8f96b61a246ba9b5a35ecbd3ed20e.png)

Completing Advent of Cyber as an organizationWith TryHackMe’s [management dashboard for business](https://tryhackme.com/business), you can do the following:

* Get full access to all of TryHackMe’s rooms and features, including Advent of Cyber
* Leverage competitive learning and collectively engage your team in Advent of Cyber tasks, measuring their progress
* Create customised learning to deep dive into further training topics based on Advent of Cyber and beyond We’re also running a limited set of enterprise training webinars for TryHackMe for Business clients to deeply explore some of the topics that we are running through the Advent of Cyber. More details about this to follow as the event starts!

If you’re interested in exploring the plethora of business benefits TryHackMe brings, reach out to [sales@tryhackme.com](mailto:sales@tryhackme.com) for a week-long free trial of the management dashboard. Customise paths and track your team’s progress for heightened engagement, enjoyment, and cyber security strength!

If you’re an existing client and want to get your wider team and company involved, please reach out to your dedicated customer success manager!

## Questions

1. Read the above.
