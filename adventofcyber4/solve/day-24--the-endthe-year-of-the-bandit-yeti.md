
# Task#30 - [Day 24] The EndThe Year of the Bandit Yeti

[Nightmare Before Elfmas - The End]
![McSkidy watches Santa's sleigh taking off while drinking hot chocolate](/tryhackme.com/adventofcyber4/file/94830bff9ac15e679a41d820b4414e82.png)

Ah, what a month! As McSkidy watched Santa’s sleigh take off, loaded with gifts, she sighed with relief.   
We did it!   



![McSkidy walks through Santa's workshop while other elves go about their pending tasks](/tryhackme.com/adventofcyber4/file/986c561fe48ff1bcd73f0fd2e4a24b8b.png)  



Looking around the workshop, she could see Santa’s SOC Team working on their tasks.   
Some were setting up additional defenses, some were implementing new security policies,   
and some were trying out new skills (and hats!) too.


![The Bandit Yeti walks away from Santa's compund](/tryhackme.com/adventofcyber4/file/03eb8181e1c70490027eba487ed0a092.png)

There are some things McSkidy can’t see, but would be vital for you to know.   



![The Bandit Yeti tears down the board with his 2022 evil plan](/tryhackme.com/adventofcyber4/file/248aec616890f5b7727a41d70e08fc32.png)  



The Bandit Yeti has left the area and returned to his lair, defeated for now.  


![The Yeti crosses out 2022 and writes 2023 to start off planning for next year](/tryhackme.com/adventofcyber4/file/a8064e6db7bb810884ac871b79cab65d.png)  



If we could look inside his planning room, we’d see the beginnings   
of a new scheme, but let’s not worry about that today!


![McSkidy enters the SOC room](/tryhackme.com/adventofcyber4/file/90602a5c3976a61dbfa12ec75b0157ea.png)

As McSkidy returned to her office, she looked at her desk,   
where just 24 days ago, an evil-looking card was placed.   
Now that spot was occupied by a scroll with a massive security to-do list. 


![McSkidy unrolls the scroll with the security to-do list](/tryhackme.com/adventofcyber4/file/ec1548addd9ede507fcfbf486353a602.png)

They all worked hard to clear as many items as possible, but many remain.   
Security is never done!  


![McSkidy crosses out the Save Christmas item off the list](/tryhackme.com/adventofcyber4/file/aeb03cb85124ffcc48ba73c9ba8334f6.png)

However, with Santa in the air, she could cross out “Save Christmas” off the list. Success!  



![McSkidy and the whole Elf team celebrate victory with a nice Christmas meal](/tryhackme.com/adventofcyber4/file/99ebd7040adfbed402e526a9e2f15a68.png)

McSkidy and all the Elves from Santa’s Security Team thank you for your help this year.   
They promised to call you if they get into trouble in 2023!

[The Final Countdown]You have until the end of December 27th to answer as many questions in this room as possible. Each question answered is an additional ticket in the main prize raffle! We will announce prize winners on December 28th. See the timer below for the exact countdown!


![](https://gen.sendtric.com/countdown/p1w0poa7nw)The Advent of Cyber 2022 room will remain available for you to explore. Grab the last question of the event below and get one more raffle ticket! Please note that once all questions in this room are answered, you will receive an Advent of Cyber 2022 badge and certificate. Very cool! Check your Full Name on your [profile page](https://tryhackme.com/profile) before generating the Certificate, as it can’t be changed afterwards.  


Happy Holidays,

*The TryHackMe Team*  




## Questions

1. Are you ready to continue your learning journey on TryHackMe? (Yea,Yea)

