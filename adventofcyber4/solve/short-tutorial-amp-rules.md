
# Task#2 - Short Tutorial & Rules

![best festival company](/tryhackme.com/adventofcyber4/file/4d1ce62bfe01f864b6f384b9a6210f74.png)  
Short Tutorial New tasks are released daily at random times between 12pm GMT and 8pm GMT, with the first challenge releasing on December 1st. They will vary in difficulty (although they will always be aimed at a beginner level).

Each task in the event will include instructions on interacting with the practical material. Please follow them carefully! Below are some general guidelines for getting connected:  

* We highly recommend you complete the [Tutorial](https://tryhackme.com/room/tutorial) room to learn more about getting connected.
* To access some target machines you deploy on TryHackMe, you will need to either use an OpenVPN client or deploy your own web-based AttackBox.
* You can deploy the AttackBox by clicking the blue "Start AttackBox" button at the top of this page.
* Using the web-based AttackBox, you can complete exercises through your browser. If you're a regular user, you can deploy the AttackBox **for free for 1 hour a day**; if you're [subscribed](https://tryhackme.com/profile#subscribe), you can deploy it for an unlimited amount of time!
* One of the tasks in the event will require you to deploy the Kali Linux VM instead of the AttackBox. The Kali Linux VM will be free under the same rules as the AttackBox during the Advent of Cyber event.
* Some tasks will ask you to only deploy a VM without spawning the AttackBox.
* Some tasks will feature sites to interact with - in this case, launching the website using an appropriate button and opening it in split view will be enough.

## Rules  

Breaking any of the following rules will result in elimination from the event:* .tryhackme.com and the OpenVPN server are off-limits to probing, scanning, or exploiting.

* Users are only authorized to hack machines deployed in the rooms they have access to.
* Users are not to target or attack other users.
* Users should only enter the event once, using one account.
* Answers to questions are not to be shared; unless shown on videos/streams.

## Questions

1. Practice connecting to our network!
