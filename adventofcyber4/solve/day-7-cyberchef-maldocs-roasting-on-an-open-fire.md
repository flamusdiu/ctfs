# Task#12 - [Day 7] CyberChef Maldocs roasting on an open fire

The Story  

![Shows AOC day 7 Image Head](/tryhackme.com/adventofcyber4/file/02a402283ddc03c2afd62b0f31f722b3.png)

Check out SecurityNinja's video walkthrough for Day 7 [here](https://www.youtube.com/watch?v=W4dZW5s2CeA)!  

In the previous task, we learned that McSkidy was indeed a victim of a spearphishing campaign that also contained a suspicious-looking document `Division_of_labour-Load_share_plan.doc`. McSkidy accidentally opened the document, and it's still unknown what this document did in the background. McSkidy has called on the in-house expert **Forensic McBlue** to examine the malicious document and find the domains it redirects to. Malicious documents may contain a suspicious command to get executed when opened, an embedded malware as a dropper (malware installer component), or may have some C2 domains to connect to.  

Learning Objectives

![Cyberchef logo](/tryhackme.com/adventofcyber4/file/e00238f51f4e22b052fba6c422c3423d.png)

* What is CyberChef
* What are the capabilities of CyberChef
* How to leverage CyberChef to analyze a malicious document
* How to deobfuscate, filter and parse the data
  
## Lab Deployment

For today's task, you will need to deploy the machine attached to this task by pressing the green "**Start Machine**" button located at the top-right of this task. The machine should launch in a split-screen view. If it does not, you will need to press the blue "Show Split View" button near the top-right of this page.

## CyberChef Overview

CyberChef is a web-based application - used to slice, dice, encode, decode, parse and analyze data or files. The CyberChef layout is explained below. An offline version of cyberChef is bookmarked in Firefox on the machine attached to this task.

![CyberChef Interface](/tryhackme.com/adventofcyber4/file/47c75a03ac04b0c2922a1cbbcefad496.png)

1. Add the text or file in panel 1.
2. Panel 2 contains various functions, also known as recipes that we use to encode, decode, parse, search or filter the data.
3. Drag the functions or recipes from Panel 2 into Panel 3 to create a recipe.
4. The output from the recipes is shown in panel 4.
5. Click on bake to run the functions added in Panel 3 in order. We can select AutoBake to automatically run the recipes as they are added.

## Using CyberChef for mal doc analysis

Let's utilize the functions, also known as recipes, from the left panel in CyberChef to analyze the malicious doc. Each step is explained below:

### 1) Add the File to CyberChef

Drag the invoice.doc file from the desktop to panel 1 as input, as shown below. Alternatively, the user can add the `Division_of_labour-Load_share_plan.doc` file by Open file as input icon in the top-right area of the CyberChef page.  

![Shows how to drag a file into CyberChef as input](/tryhackme.com/adventofcyber4/file/44901d6b3afd7c63acf7c9c0c9c3e18b.gif)  

### 2) Extract strings

Strings are ASCII and Unicode-printable sequences of characters within a file. We are interested in the strings embedded in the file that could lead us to suspicious domains. Use the `strings` function from the left panel to extract the strings by dragging it to panel 3 and selecting **All printable chars** as shown below:  

![Extract strings using strings function](/tryhackme.com/adventofcyber4/file/12b35d2dcb21944881d978f6f65e8d42.gif)  

If we examine the result, we can see some random strings of different lengths and some obfuscated strings. Narrow down the search to show the strings with a larger length. Keep increasing the minimum length until you remove all the noise and are only left with the meaningful string, as shown below:  

![Filter strings by the size using strings function](/tryhackme.com/adventofcyber4/file/6e8e2cb719260599ed8a51843b34fa19.png)  

### 3) Remove Pattern

Attackers often add random characters to obfuscate the actual value. If we examine, we can find some repeated characters `[ _ ]`. As these characters are common in different places, we can use regex **(regular expressions)** within the `Find / Replace` function to find and remove these repeated characters.

To use regex, we will put characters within the square brackets `[ ]` and use backslash `\` to escape characters. In this case, the final regex will be `[**\[\]\n\_**]` where `\n` represents **the Line feed**, as shown below:

![Use Regex in Find/Replace to filter data](/tryhackme.com/adventofcyber4/file/8c0b0f724002e6dc8457d8b7f095c486.png)  

It's evident from the result that we are dealing with a PowerShell script, and it is using base64 Encoded string to hide the actual code.  

### 4) Drop Bytes

To get access to the base64 string, we need to remove the extra bytes from the top. Let's use the `Drop bytes` function and keep increasing the number until the top bytes are removed.

![Use drop bytes to drop unwanted bytes](/tryhackme.com/adventofcyber4/file/c1e177a5656640e25ef0c5a83990f8a7.png)  

### 5) Decode base64

Now we are only left with the base64 text. We will use the `From base64` function to decode this string, as shown below:

![Use from Base64 to decode text from Base84 encoded value](/tryhackme.com/adventofcyber4/file/309cbbd50b3b5e27e17677f96f04b069.png)  

### 6) Decode UTF-16

The base64 decoded result clearly indicates a PowerShell script which seems like an interesting finding. In general, the PowerShell scripts use the `Unicode UTF-16LE` encoding by default. We will be using the `Decode text` function to decode the result into UTF-16E, as shown below:

![Decode to UTF-16LE using decode text function](/tryhackme.com/adventofcyber4/file/0faa8f5668d9d998696aa8547d80c3b7.png)  

### 7) Find and Remove Common Patterns

Forensic McBlue observes various repeated characters  ``' ( ) + ` "`` within the output, which makes the result a bit messy. Let's use regex in the `Find/Replace`function again to remove these characters, as shown below. The final regex will be ``[`'()+"]``.  

![Use regex within Find/Replace to filter data](/tryhackme.com/adventofcyber4/file/da469d1dabcf929dbc1765d06917521d.png)  

### 8) Find and Replace

If we examine the output, we will find various domains and some weird letters `]b2H_` before each domain reference. A replace function is also found below that seems to replace this `]b2H_` with `http`. ![Shows usage of Find/Replace function](/tryhackme.com/adventofcyber4/file/938334f69a64ce058bd2046da3928114.png)  

Let's use the `find / Replace` function to replace `]b2H_` with `http` as shown below:

![Use Replace/Replace function to replace chars](/tryhackme.com/adventofcyber4/file/d71d6682328beec84e0948a6ade15c69.png)  

### 9) Extract URLs

The result clearly shows some domains, which is what we expected to find. We will use the `Extract URLs` function to extract the URLs from the result, as shown below:

![Use Extract URLs function to extract URLs from the data](/tryhackme.com/adventofcyber4/file/76d6badf89e2022315851487435f12f6.png)  

### 10) Split URLs with @

The result shows that each domain is followed by the `@` character, which can be removed using the split function as shown below:

![Use split function to split lines](/tryhackme.com/adventofcyber4/file/7f2239c965339cf309d101f6c8d713eb.png)  

### 11) Defang URL

Great - We have finally extracted the URLs from the malicious document; it looks like the document was indeed malicious and was downloading a malicious program from a suspicious domain.

Before passing these domains to the SOC team for deep malware analysis, it is recommended to defang them to avoid accidental clicks. Defanging the URLs makes them unclickable. We will use `Defang URL` to do the task, as shown below:  

![Use Defang to defang URLs to make them unclickable](/tryhackme.com/adventofcyber4/file/58a58436becb51b25dbb13ebe56b9a02.png)  

## Great work!

It's time to share the URLs and the malicious document with the Malware Analysts.

## Questions

1. What is the version of CyberChef found in the attached VM? **9.49.0**

2. How many recipes were used to extract URLs from the malicious doc? **10**

    > **Recipe URL**: file:///home/ubuntu/Downloads/CyberChef_v9.49.0/CyberChef_v9.49.0.html#recipe=Strings('Single%20byte',258,'All%20printable%20chars%20(A)',false,false,false)Find_/_Replace(%7B'option':'Regex','string':'%5B%5C%5C%5B%5C%5C%5D%5C%5Cn_%5D'%7D,'',true,false,true,false)Drop_bytes(0,124,false)From_Base64('A-Za-z0-9%2B/%3D',true,false)Decode_text('UTF-16LE%20(1200)')Find_/_Replace(%7B'option':'Regex','string':'%5B()%5C'%60%22%2B%5D'%7D,'',true,false,true,false)Find_/_Replace(%7B'option':'Regex','string':'%5Db2H_'%7D,'http',true,false,true,false)Extract_URLs(false,false,false)Split('@','%5C%5Cn')Defang_URL(true,true,true,'Valid%20domains%20and%20full%20URLs')

3. We found a URL that was downloading a suspicious file; what is the name of that malware? **mysterygift.exe**

    **Hint:** Provide a non-defanged output.

4. What is the last defanged URL of the bandityeti domain found in the last step?  **`hxxps[://]cdn[.]bandityeti[.]THM/files/index/`**

5. What is the ticket found in one of the domains? (Format: Domain/<GOLDEN\_FLAG>)  **THM_MYSTERY_FLAG**

6. If you liked the investigation today, you might also enjoy the [Security Information and Event Management](https://tryhackme.com/module/security-information-event-management) module!
