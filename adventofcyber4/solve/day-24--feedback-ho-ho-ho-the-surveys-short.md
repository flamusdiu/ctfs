# Task#29 - [Day 24] Feedback Ho, ho, ho, the survey's short

![Banner image showing a Christmas garland with the number 24 and different ornaments including a candy-cane question mark a medal a survey notepad and a finish line](/tryhackme.com/adventofcyber4/file/1cac385aec8b539fbcb2841e4f72edae.png)
[Thank You]Thank you so much for participating in this year's Advent of Cyber event!

## Questions

1. Please help us improve by answering [this 5-minute survey](https://forms.gle/grAqhDAmE7i33drG8). Make sure to grab the flag before you click "Submit"! **THM{AoC2022!thank_you!}**

    **Hint:** Complete the form to get the answer to this question. Make sure to grab the flag before you click "Submit"!

2. Continue learning with the [Pre Security](https://tryhackme.com/path/outline/presecurity), [Jr Penetration Tester](https://tryhackme.com/path/outline/jrpenetrationtester), or [SOC Level 1](https://tryhackme.com/path/outline/soclevel1) pathway!  

3. The prize winners will be announced on the 28th of December - you have until then to complete the tasks. Remember, the more questions you answer, the higher your chance of winning!
  
The daily prize winners for the last week of the event will be announced on Twitter on Wednesday, December 28th.
