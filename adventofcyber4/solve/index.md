# Advent of Cyber 2022

Get started with Cyber Security in 24 Days - learn the basics by doing a new, beginner-friendly security challenge every day leading up to Christmas.

<object id="yt-vid" width="100%" height="600px" data="https://www.youtube.com/embed/eib8zGMTook"></object><br />YT: [Advent of Cyber 2022 Day #23](https://www.youtube.com/embed/eib8zGMTook)

## Certificate

![certificate](images/2022-12-23-18-06-43.png)

[Completing Advent of Cyber 4!](https://tryhackme.com/azuleonyx/badges/adventofcyber4)

## Tasks

- [T#1 Introduction](introduction.md)
- [T#2 Short Tutorial &amp; Rules](short-tutorial-amp-rules.md)
- [T#3 Our Socials](our-socials.md)
- [T#4 Subscribing, TryHackMe for Business &amp; Christmas Swag!](subscribing-tryhackme-for-business-amp-christmas-swag.md)
- [T#5 Nightmare Before Elfmas - The Story](nightmare-before-elfmas--the-story.md)
- [T#6 [Day 1] **Frameworks** Someone's coming to town!](day-1-frameworks-someones-coming-to-town.md)
- [T#7 [Day 2] **Log Analysis** Santa's Naughty &amp; Nice Log](day-2-log-analysis-santas-naughty-amp-nice-log.md)
- [T#8 [Day 3] **OSINT** Nothing escapes detective McRed](day-3-osint-nothing-escapes-detective-mcred.md)
- [T#9 [Day 4] **Scanning** Scanning through the snow](day-4-scanning-scanning-through-the-snow.md)
- [T#10 [Day 5] **Brute-Forcing** He knows when you're awake](day-5-bruteforcing-he-knows-when-youre-awake.md)
- [T#11 [Day 6] **Email Analysis** It's beginning to look a lot like phishing](day-6-email-analysis-its-beginning-to-look-a-lot-like-phishing.md)
- [T#12 [Day 7] **CyberChef** Maldocs roasting on an open fire](day-7-cyberchef-maldocs-roasting-on-an-open-fire.md)
- [T#13 [Day 8] **Smart Contracts** Last Christmas I gave you my ETH](day-8-smart-contracts-last-christmas-i-gave-you-my-eth-.md)
- [T#14 [Day 9] **Pivoting** Dock the halls](day-9-pivoting-dock-the-halls-.md)
- [T#15 [Day 10] **Hack a game** You're a mean one, Mr. Yeti](day-10-hack-a-game-youre-a-mean-one-mr.-yeti.md)
- [T#16 [Day 11] **Memory Forensics** Not all gifts are nice](day-11-memory-forensics-not-all-gifts-are-nice.md)
- [T#17 [Day 12] **Malware Analysis** Forensic McBlue to the REVscue!](day-12-malware-analysis-forensic-mcblue-to-the-revscue.md)
- [T#18 [Day 13] **Packet Analysis** Simply having a wonderful pcap time](day-13-packet-analysis-simply-having-a-wonderful-pcap-time-.md)
- [T#19 [Day 14] **Web Applications** I'm dreaming of secure web apps](day-14-web-applications-im-dreaming-of-secure-web-apps.md)
- [T#20 [Day 15] **Secure Coding** Santa is looking for a Sidekick](day-15-secure-coding-santa-is-looking-for-a-sidekick.md)
- [T#21 [Day 16] **Secure Coding** SQLi’s the king, the carolers sing](day-16-secure-coding-sqli’s-the-king-the-carolers-sing.md)
- [T#22 [Day 17] **Secure Coding** Filtering for Order Amidst Chaos](day-17-secure-coding-filtering-for-order-amidst-chaos.md)
- [T#23 [Day 18] **Sigma** Lumberjack Lenny Learns New Rules](day-18-sigma-lumberjack-lenny-learns-new-rules.md)
- [T#24 [Day 19] **Hardware Hacking** Wiggles go brrr](day-19-hardware-hacking-wiggles-go-brrr.md)
- [T#25 [Day 20] **Firmware**  Binwalkin’ around the Christmas tree](day-20-firmware--binwalkin’-around-the-christmas-tree-.md)
- [T#26 [Day 21] **MQTT** Have yourself a merry little webcam](day-21-mqtt-have-yourself-a-merry-little-webcam-.md)
- [T#27 [Day 22] **Attack Surface Reduction** Threats are failing all around me](day-22-attack-surface-reduction-threats-are-failing-all-around-me.md)
- [T#28 [Day 23] **Defence in Depth** Mission ELFPossible: Abominable for a Day](day-23-defence-in-depth-mission-elfpossible-abominable-for-a-day.md)
- [T#29 [Day 24]  **Feedback** Ho, ho, ho, the survey's short](day-24--feedback-ho-ho-ho-the-surveys-short.md)
- [T#30 [Day 24]  **The End**The Year of the Bandit Yeti](day-24--the-endthe-year-of-the-bandit-yeti.md)
