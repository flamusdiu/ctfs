# CTFs

List of CTF write-ups.

- [Magnet Forensics User Summit 2021][mvs]
  - [https://mvs2021ctf.ctfd.io][mvs2021ctf]
  - [Writeup][mvs-writeup]

- [Operation: Safe Escape][ose]
  - [https://https://tryhackme.com/room/osefundraisingctf][thm-ose]
  - [Writeup][thm-ose-writeup]

- [MetaCTF CyberGames 2021][metactf]
  - [https://metactf.com/cybergames][metactf-cybergames]
  - [Writeup][metactf-cybergames-writeup]

- [OpenSecurity Holiday CTF 2021][opensecurity]
  - [https://ctf.opensecurity.io/][opensecurity-ctf]
  - [Writeup][opensecurity-holiday-ctf-writeup]

- [TryHackMe][tryhackme]
  - [TryHackMe Advent of Cyber 4][tryhackme-adventofcyber4-ctf]
  - [Writeup][tryhackme-adventofcyber4-ctf-writeup]

[mvs]: https://www.magnetforensics.com/mvs/ "Magnet Forensics User Summit"
[mvs2021ctf]: https://mvs2021ctf.ctfd.io "MVS 2021"
[mvs-writeup]: mvs2021ctf&#46;ctfd&#46;io/solve/index.md "MVS 2021 CTF Writeup"
[ose]: https://safeescape.org/ "Operation: Safe Escape"
[thm-ose]: https://https://tryhackme.com/room/osefundraisingctf "OSE Fundraising CTF"
[thm-ose-writeup]: thm-osefundraisingctf/solve/index.md "Operation: Safe Escape Writeup"
[metactf]: https://metactf.com/ "MetaCTF"
[metactf-cybergames]: https://metactf.com/cybergames "MetaCTF CyberGames"
[metactf-cybergames-writeup]: metactf2021/solve/index.md "MetaCTF CyberGames 2021 Writeup"
[opensecurity]: https://opensecurity.io/ "Open Security"
[opensecurity-ctf]: https://ctf.opensecurity.io/ "Open Security CTF"
[opensecurity-holiday-ctf-writeup]: opensecurity-holidayctf-2021/solve/index.md "Open Security CTF 2021 Writeup"
[tryhackme]: https://tryhackme.com "TryHackMe"
[tryhackme-adventofcyber4-ctf]: https://tryhackme.com/room/adventofcyber4 "TryHackMe: Advent of Cyber 4"
[tryhackme-adventofcyber4-ctf-writeup]: adventofcyber4/solve/index.md "TryHackMe: Advent of Cyber 4 Writeup"
