
# Q#58 - Good Advice (5)

## Challenges

![challenge](../../qn/58.png)

![GoodAdvice.png](../../file/GoodAdvice.jpg)

Flag will be in format FLAG(something)

### Hints

**None**

### Files

**None**

## Solve

You can decode the QR code online @ [ZXing Decoder Online](https://zxing.org/w/decode.jspx).

![qr decode](images/qr-decode.png)

Decode the bas64:

```bash
$ echo "RkxBRyhQcmVwYXJhdGlvbikgaXMgdGhlIGtleSB0byBzdWNjZXNzLg==" | base64 -d
FLAG(Preparation) is the key to success.%
```

## Answer

FLAG(Preparation)
