
# Q#61 - Comes Before Time (5)

## Challenges

![challenge](../../qn/61.png)

1618091305

### Hints

**None**

### Files

**None**

## Solve

Missed this one, though, there are a few ways to solve it.

At first, it looks like a timestamp and if you decode it, you will get a proper time. However, it's **not** a timestamp. There is no context for the number to really be a timestamp.

Evandrix had an interesting way to look at it as a Wheel of Fortune challenge: ____ Time then basically using A-Z as 1-26. You just need to split each pair of number and solve for each letter.

| <!-- --> | <!-- --> | <!-- --> | <!-- --> | <!-- --> |
| -------- | -------- | -------- | -------- | -------- |
| 16       | 18       | 09       | 13       | 05       |
| p        | r        | i        | m        | e        |

Stark4n6's [solution](https://www.stark4n6.com/2021/05/mvs2021-ctf-hunt.html) uses a A1Z26 decoder. Same as above but more automatic solution.

## Answer

prime
