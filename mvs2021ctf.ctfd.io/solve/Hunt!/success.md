
# Q#59 - Success (5)

## Challenges

![challenge](../../qn/59.png)

PJGXI zs t kvngrgiei egkcqxek htrnwifq ufv tlvcmzcaxt gdmdugqqnizsc. Hvrx, es hhv mi tf egkcqt wppg(khbvuf4j2wmcd).

### Hints

**None**

### Files

**None**

## Solve

Looking at the structure, it appears to be a single substitute, but a test with ROT13 on CyberChef did not work. The only other common substitute is the [Vigenère](https://en.wikipedia.org/wiki/Vigen%C3%A8re_cipher) cypher.

The trick to solving this is that "wppg(khbvuf4j2wmcd)" is the flag. "wppg" should equal "flag" but with two p's this must be a Vigenère cypher.

You can use [dcode.xyz](dCode.xyz)'s decoder on the [Vigenère Cipher](https://www.dcode.fr/vigenere-cipher)

![decode](images/success-decode.png)

As shown, above, if you put in "FLAG" as known plaintext word, It will crack the code.

## Answer

things4u2find
