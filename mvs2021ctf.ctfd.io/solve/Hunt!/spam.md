
# Q#63 - Spam (10)

## Challenges

![challenge](../../qn/63.png)

Dear Business person , Especially for you - this cutting-edge 
intelligence ! This is a one time mailing there is 
no need to request removal if you won't want any more 
. This mail is being sent in compliance with Senate 
bill 2416 ; Title 7 ; Section 309 . This is a ligitimate 
business proposal . Why work for somebody else when 
you can become rich inside 14 DAYS ! Have you ever 
noticed nearly every commercial on television has a 
.com on in it & more people than ever are surfing the 
web ! Well, now is your chance to capitalize on this 
! WE will help YOU SELL MORE and SELL MORE ! You are 
guaranteed to succeed because we take all the risk 
! But don't believe us ! Prof Anderson of Indiana tried 
us and says "I was skeptical but it worked for me" 
! We assure you that we operate within all applicable 
laws ! You have no reason not to act now ! Sign up 
a friend and you get half off . Thanks ! 

### Hints

- Hint [#1](../../hint/1.json) (Cost 3): Maybe it's mimicing spam?
- Hint #2 (Cost 4): **Locked**

### Files

**None**

## Solve

Perform a internet search for "Senate bill 2416 ; Title 7 ; Section 309" and you will find a will see a page on Forums of Loathing.

[Forums of Loathing: The hidden Message thread](http://forums.kingdomofloathing.com/vb/showthread.php?t=84398)

The page has the following text which is similar to the challenge.

```txt
Ok Simple. You go Here<http://www.spammimic.com/> and click encode
put in a short message thats non flame and is acceptable here. Then next poster says the answer and does their own.

Dear Colleague , Thank-you for your interest in our
newsletter ! If you no longer wish to receive our publications
simply reply with a Subject: of "REMOVE" and you will
immediately be removed from our mailing list ! This
mail is being sent in compliance with Senate bill 1627
, Title 7 ; Section 309 . This is not a get rich scheme
. Why work for somebody else when you can become rich
within 27 months . Have you ever noticed how long the
line-ups are at bank machines & people love convenience
! Well, now is your chance to capitalize on this !
WE will help YOU SELL MORE and process your orders
within seconds ! The best thing about our system is
that it is absolutely risk free for you ! But don't
believe us . Mrs Simpson of Maryland tried us and says
"I was skeptical but it worked for me" . We assure
you that we operate within all applicable laws ! We
implore you - act now ! Sign up a friend and you get
half off . Thanks . 
```

If you click the link, it takes you to [spam mimic](http://www.spammimic.com/). 

![spam decode](images/spam-decode.png)

## Answer

robotsarebad
