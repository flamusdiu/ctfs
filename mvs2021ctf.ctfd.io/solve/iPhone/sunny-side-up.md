
# Q#5 - Sunny Side Up (5)

## Challenges

![challenge](../../qn/5.png)

How does John like his eggs? (2 words)

### Hints

**None**

### Files

**None**

## Solve

Searching AXIOM for 'eggs' shows:

![eggs](images/iphone-tiktok-eggs.png)

## Answer

chicken form
