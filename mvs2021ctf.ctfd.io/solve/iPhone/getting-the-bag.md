
# Q#8 - Getting the Bag (10)

## Challenges

![challenge](../../qn/8.png)

When was the first time Eli got Chipotle? mm/dd/yyyy

### Hints

**None**

### Files

**None**

## Solve

Using Apollo, there are only one data point which shows up next to a Chipotle. 

![apollo](images/iphone-maps.png)

![chipotle](images/iphone-chipotle.png)

## Answer

02/12/2021
