
# Q#12 - The Epitome of Health (25)

## Challenges

![challenge](../../qn/12.png)

What time did the health database last sync? Answer in GMT and HH:MM:SS format

### Hints

**None**

### Files

**None**

## Solve

Health DB Information: [The iOS of Sauron:How iOS Tracks Everything You Do](https://raw.githubusercontent.com/mac4n6/Presentations/master/iOS%20of%20Sauron%20-%20How%20iOS%20Tracks%20Everything%20You%20Do/iOS_of_Sauron_04162016.pdf) by Sarah Edwards (@mac4n6)

- Health-/private/var/mobile/Library/Health/
  - healthdb.sqlite(11 Tables)
  - healthdb_secure.sqlite(16 Tables

The sync information is in the `healthdb.sqlite` file on the `cloud_sync_stores` table.

![cloud sync](images/iphone-health-db.png)

Using this time, you need to add 978307200 (number of seconds since 1/1/1970) to the timestamp in the database.

![cyberchef](images/health-cyberchef-conversion.png)

## Answer

05:35:53
