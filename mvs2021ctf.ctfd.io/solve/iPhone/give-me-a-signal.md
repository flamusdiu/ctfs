
# Q#13 - Give me a signal (25)

## Challenges

![challenge](../../qn/13.png)

What was the link sent to Eli on Signal?

### Hints

**None**

### Files

**None**

## Solve

In AXIOM, look under Signal Messages (must be decrypted first):

![link](images/iphone-signal-link.png)

## Answer

<https://vm.tiktok.com/ZMejtu5mG/>
