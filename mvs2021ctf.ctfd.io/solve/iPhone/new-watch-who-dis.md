
# Q#6 - New Watch Who Dis (5)

## Challenges

![challenge](../../qn/6.png)

What is the MAC address of Eli's apple watch?

### Hints

**None**

### Files

**None**

## Solve

![bluetooth](images/iphone-bluetooth.png)

## Answer

50:A6:7F:8F:A5:B6
