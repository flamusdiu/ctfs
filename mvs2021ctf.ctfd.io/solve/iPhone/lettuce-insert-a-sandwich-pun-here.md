
# Q#65 - Lettuce insert a sandwich pun here (50)

## Challenges

![challenge](../../qn/65.png)

Eli was telling his friend about a sandwich he got.  When was the message sent where he said he got the sandwich? Answer in yyyy-mm-dd HH:MM:SS GMT

### Hints

**None**

### Files

**None**

## Solve

```bash
$ grep -sori "sandwich" .
```

![lettuce search](images/lettuce-grep-search.png)

Examining the file in 010 Editor:

![010 editor](images/lettuce-010-editor.png)

The first few bits, and structure of the file appears to be "protobuf." Through, it into CyberChef.

![cyberchef](images/cyberchef-proto-buf.png)

Scrolling down in the decode, you find the string, and below that appears to be a Unit Timestamp in milliseconds.

Gives: `Thu 4 March 2021 21:33:11.699 UTC`

## Answer

2021-03-04 21:33:11
