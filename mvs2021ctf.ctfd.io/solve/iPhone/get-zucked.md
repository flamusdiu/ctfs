
# Q#7 - Get Zucked! (5)

## Challenges

![challenge](../../qn/7.png)

What is Eli's facebook password?

### Hints

**None**

### Files

**None**

## Solve

![facebook](images/facebook-password.png)

## Answer

fix_my_flatt2!
