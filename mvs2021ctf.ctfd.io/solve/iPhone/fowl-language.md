
# Q#23 - Fowl language (25)

## Challenges

![challenge](../../qn/23.png)

Who was mentioned outside the Chick-Fil-A?

### Hints

**None**

### Files

**None**

## Solve

Source: `518e8d766f9b3e76db216f35fdb6b0604e50f61b_files_full.zip\private\var\mobile\Media\DCIM\100APPLE\IMG_0003.HEIC`

In Axiom, there are several images from outside Chick-fil-a, take from the iPhone.

![chick-fil-a](images/chick-fil-a-sounds.png)

Going to the time line from the "Created Time" in the metadata of the image:

![timelines](images/chick-fil-a-timeline.png)

You find a video which has sound that can be played which has the flag.

## Answer

Jonathan
