
# Q#16 - Breaking Quarntine (5)

## Challenges

![challenge](../../qn/16.png)

When does Eli go to a neighboring state? Answer in MM/DD/YYYY

### Hints

**None**

### Files

**None**

## Solve

Using APOLLO, loading up `routined_cloud_visit_inbound_start.kmz` file into Google Earth and you get the following map (marked up outside of Google Earth).

![maps](images/iphone-maps.png)

## Answer

02/20/2021
