
# Q#17 - Burger Time (5)

## Challenges

![challenge](../../qn/17.png)

What fast food restaurant has an application is installed on the device?

### Hints

**None**

### Files

**None**

## Solve

Looking in `ios_app` at the `Apps` table:

![apps](images/iphone-apps.png)

The only application matching "fast food" is Chick-fil-A.

## Answer

Chick-fil-A
