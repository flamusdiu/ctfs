
# Q#18 - Beefstew isn't a Stoganoff Password (10)

## Challenges

![challenge](../../qn/18.png)

How many Apple Notes did Eli Encrypt?

### Hints

**None**

### Files

**None**

## Solve

Source: AXOIM's Documents > Apple Notes

![notes](images/iphone-notes-enc.png)

## Answer

3
