
# Q#15 - TikTokClock (75)

## Challenges

![challenge](../../qn/15.png)

When was the tiktok sent in signal posted? Answer in yyyy-mm-dd hh:mm:ss GMT

### Hints

**None**

### Files

**None**

## Solve

![axiom](images/tiktok-link-signel-axiom.png)

Using, ExpandURL, you can see the redirection. However, the service only shows halfway to the page. In screenshot below, you can use the red, green, and purple lines to match up to the actual link you would get in your browser.

![expand url](images/expand-url-tiktok-signal-link.png)

According to Ryan's post, below, we need the number at the end url in the browser or after the `/v/` in the expanded url which is `6903950172120730885`. Note this is the same number in the query parms for `share_item_id` in the expanded URL.

Read: [Tinkering with TikTok Timestamps](https://dfir.pubpub.org/pub/9llea7yp/release/1) by Ryan Benson

Use (link shows solution): [Cyberchef](https://gchq.github.io/CyberChef/#recipe=To_Base(2)Regular_expression('User%20defined','(%5E0?%5C%5Cd%7B31%7D)',true,true,false,false,false,false,'List%20matches')From_Base(2)Translate_DateTime_Format('UNIX%20timestamp%20(seconds)','X','UTC','YYYY-MM-DD%20HH:mm:ss','UTC')&input=NjkwMzk1MDE3MjEyMDczMDg4NQ)

```html
#recipe=To_Base(2)Regular_expression('User%20defined','(%5E0?%5C%5Cd%7B31%7D)',
true,true,false,false,false,false,'List%20matches')From_Base(2)Translate_DateTime_Format
('UNIX%20timestamp%20(seconds)','X','UTC','YYYY-MM-DD%20HH:MM:SS','UTC')&
input=NjkwMzk1MDE3MjEyMDczMDg4NQ
```

![cyberchef](images/cyberchef-tiktok-conversion.png)

### Second Solve

Just realized that this can be easier done in [unfurl](https://dfir.blog/unfurl/). (Source: [stark4n6](https://www.stark4n6.com/2021/05/mvs2021-ctf-iphone.html))

![unfurl](images/tiktok-unfurl.png)

## Answer

2020-12-08 18:12:00
