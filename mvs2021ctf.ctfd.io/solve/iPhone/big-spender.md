
# Q#9 - Big Spender (10)

## Challenges

![challenge](../../qn/9.png)

How much (after tax) was Eli's Chick-fil-A order? Exclude Dollar sign

### Hints

**None**

### Files

**None**

## Solve

Looking at iLEAPP, you find the email, however, the message is cut off.

![email](images/iphone-email.png)

To find the rest of the email, you need to open the `Envelope Index` at `private\var\mobile\Library\Mail` in `DB Browser for SQLite`.

Location of emails: `private\var\mobile\Library\Mail\MessageData`

```bash
$ cd "private\var\mobile\Library\Mail\MessageData"
$ grep -ri 'chick-fil-a' | grep -i 'receipt'
41/partial.emlx:Subject: =?UTF-8?Q?Chick-fil-A=C2=AE_Mobile_Ordering_Receipt?=
```

The `41` shows which folder in the `MessageData` has the email. Examining the `1.emlxpart` email. 

![editor](images/iphone-010-editor.png)

Examining the email towards the bottom you see the total.

## Answer

27.24
