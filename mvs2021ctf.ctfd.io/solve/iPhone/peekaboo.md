
# Q#24 - Peek-a-boo (25)

## Challenges

![challenge](../../qn/24.png)

What app was used to let Eli know it is Burrito Time?

### Hints

**None**

### Files

**None**

## Solve

File: `cm-chat-media-video-zy5EhEnNZx4sAMBc4PoRj.mov`

![snapchat](images/snap-chat-mov.png)

## Answer

Snapchat
