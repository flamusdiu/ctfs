
# Q#68 - Chicken on a Sunday? (25)

## Challenges

![challenge](../../qn/68.png)

Okay, so we know Eli likes Chick-fil-A, what 2 other chain fast food restaurants were visited?  Include both in answer, formatting will not be an issue.   

* Example: DFA-Diner and Magnet Cafe 

### Hints

**None**

### Files

**None**

## Solve

Looking through the snapshots, you find:

![wendy](images/iphone-maps-wendy.png)

Looking at APOLLO, you also find location data at Chipotle:

![chipotle](images/iphone-apollo-chipotle.png)

There is also a picture of a chipotle bag which I only noticed later.

## Answer

Wendy's and Chipotle
