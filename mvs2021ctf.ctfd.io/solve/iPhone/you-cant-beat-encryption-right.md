
# Q#14 - You can't beat encryption right? (25)

## Challenges

![challenge](../../qn/14.png)

What user was Eli texting on Wickr?

### Hints

**None**

### Files

**None**

## Solve

Using AXIOM, check the Wickr messages (make sure to decrypt the messages):

![wickr](images/iphone-wickr.png)

## Answer

jchipps723
