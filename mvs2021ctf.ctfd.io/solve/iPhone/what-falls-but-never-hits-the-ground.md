
# Q#20 - What falls but never hits the ground? (15)

## Challenges

![challenge](../../qn/20.png)

What was the temperature in Burlington on March 3rd at approximately 3pm? Answer in degrees fahrenheit.

### Hints

**None**

### Files

**None**

## Solve

Completely didn't see this one until I read stark4n6's [blog](https://www.stark4n6.com/2021/05/mvs2021-ctf-iphone.html)


Checking the snapshots, you find the Wendy's image with a "27" temperature on it.

![wendy](images/iphone-maps-wendy.png)

Checking the metadata for it:

![details](images/iphone-maps-wendy-details.png)

The creation timestamp matches the challenge date/time.

## Answer

27
