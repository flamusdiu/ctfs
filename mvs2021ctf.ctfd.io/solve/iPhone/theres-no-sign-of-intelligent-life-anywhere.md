
# Q#11 - There's No Sign of Intelligent Life Anywhere (15)

## Challenges

![challenge](../../qn/11.png)

Eli was sent a flat earth meme.  Give the last 5 characters of the MD5 hash of the file.

### Hints

**None**

### Files

**None**

## Solve

Checking the media in AXIOM:

![flat earth](images/iphone-flat-earth.png)

![details](images/iphone-flat-earth-details.png)

## Answer

889aa