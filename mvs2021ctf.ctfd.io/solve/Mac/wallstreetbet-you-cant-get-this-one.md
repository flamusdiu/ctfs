
# Q#54 - WallStreetBet You Can't Get This One (50)

## Challenges

![challenge](../../qn/54.png)

Eli searched for 4 stock quotes (not a web search).  What was the second stock he searched for? answer in ticker form ex: $MVS

### Hints

**None**

### Files

**None**

## Solve

Source: `Users/eliflatt/Library/Containers/com.apple.stocks/Data/Library/Caches/com.apple.stocks/Cache.db`

![search](images/wallstreetbet-search.png)

## Answer

$SPCE
