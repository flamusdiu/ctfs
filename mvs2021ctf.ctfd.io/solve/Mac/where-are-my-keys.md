
# Q#55 - Where are my keys!? (50)

## Challenges

![challenge](../../qn/55.png)

What is the encryption-key for Eli's iCloud (SHA256)?

### Hints

**None**

### Files

**None**

## Solve

Source: `/Users/eliflatt/Library/Keychains/622523FF-A9E8-5BFC-9142-B14980A33465/keychain-2.db`

![keys](images/keys.png)

The line `com.apple.security.egoIndentities` contains the right flag. Searching online didn't yield on any information for this package name. This is a guess due to the "encryption-key" words in the challenge.

## Answer

fUpf9J+cLRI3OCJ/KdFpZoaZXgfj2DC3ZrQnW7XT9Os=
