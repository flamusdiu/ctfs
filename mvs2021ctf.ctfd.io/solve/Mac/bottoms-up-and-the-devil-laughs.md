
# Q#46 - Bottoms up, and the devil laughs (10)

## Challenges

![challenge](../../qn/46.png)

What is Eli's preferred energy drink brand?

### Hints

**None**

### Files

**None**

## Solve

In AXIOM, search for "Drink" yields a bunch of URLs related to "Bang Energy Drink"

![search](images/bottoms-search.png)

## Answer

bang
