
# Q#45 - I love it when you call me big sur (5)

## Challenges

![challenge](../../qn/45.png)

What is the Product Build Version?

### Hints

**None**

### Files

**None**

## Solve

Using `mac_apt` check the `Basic Info` table.

![product build](images/big-sur-build.png)

## Answer

20D74
