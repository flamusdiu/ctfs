
# Q#50 - Oh Sheet! (25)

## Challenges

![challenge](../../qn/50.png)

What is the name of the spreadsheet Eli often navigated to?

### Hints

**None**

### Files

**None**

## Solve

Using `mac_apt`, looking on the `Safari` table.

![sheet](images/sheet-nav.png)

Google lists the sheet names in the URLs names.

## Answer

To-Purchase
