
# Q#52 - it's ra1ning it's pouring (25)

## Challenges

![challenge](../../qn/52.png)

What is the size in bytes of the application found within Eli's trash?

### Hints

**None**

### Files

**None**

## Solve

Run the following command on the zip file.

```bash
$ unzip -l stu-21-155-171-184-20210406-134216-files.zip | grep -vi "system" | grep -i "trash"
```

unzip

- `-l` : Lists all the files which includes file size in bytes

grep

- `-vi "system"` : exclude all paths with "system" in them; case insensitive
- `-i "trash"` : include any path with "trash" in them; case insensitive

Partial output:

![file list](images/pouring-file-list.png)

DMGs are application files in macOS.

## Answer

9389392
