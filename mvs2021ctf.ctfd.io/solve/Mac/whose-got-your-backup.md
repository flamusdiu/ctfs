
# Q#44 - Whose got your back(up)? (5)

## Challenges

![challenge](../../qn/44.png)

What is the IMEI of the iOS device that is backed up?

### Hints

**None**

### Files

**None**

## Solve

Using `mac_apt`, check the `iDevice_Backups` table

![backup](images/idevice-backup.png)

## Answer

356759080486567
