
# Q#53 - Secrets Secrets are no fun (50)

## Challenges

![challenge](../../qn/53.png)

What is Eli's system password hint?

### Hints

**None**

### Files

**None**

## Solve

Using AXIOM, you can locate the password hint:

![secrets](images/secrets-eli-password.png)

You can also find it in `System/Volumes/Preboot/943CAEE3-8306-426A-A65E-4E0F4B52EBDB/var/db/CryptoUserInfo.plist`

![crypto user list](images/secrets-cryptouserinfo.png)

## Answer

fix something!
