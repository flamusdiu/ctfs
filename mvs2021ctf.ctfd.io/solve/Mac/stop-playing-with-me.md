
# Q#47 - Stop playing with me (10)

## Challenges

![challenge](../../qn/47.png)

How many websites have permissions to autoplay?

### Hints

**None**

### Files

**None**

## Solve

Source: `Users/eliflatt/Library/Safari/SitesAllowedToAutoplay.plist`

![autoplay](images/sites-autoplay.png)

## Answer

165
