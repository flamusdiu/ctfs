
# Q#49 - Remind me Later (25)

## Challenges

![challenge](../../qn/49.png)

What time and date in EST did Eli add a notification permission on Safari? Answer in MM/DD/YYYY HH:MM:SS 

### Hints

**None**

### Files

**None**

## Solve

User notifications for Safari are located in `Users\eliflatt\Library\Safari\UserNotificationPermissions.plist`.

![remind](images/remind-safari.png)

You also need to translate the time from UTC to EST as shown above. However, note that changing the timezone in AXIOM does not update the plist time displayed.

![timezone](images/remind-timezone.png)

The red arrows show the time in UTC as they match in the grid and in the plist. If you change the timezone (show in green), it will update the grid but will not update the plist.

## Answer

02/21/2021 23:15:06
