
# Q#56 - There are no penguins at the North Pole (50)

## Challenges

![challenge](../../qn/56.png)

What is the SHA1 hash of Eli's profile picture on the device?

### Hints

**None**

### Files

**None**

## Solve

Save the picture data from `System\Volumes\Preboot\943CAEE3-8306-426A-A65E-4E0F4B52EBDB\var\db\CryptoUserInfo.plist`

Opening the image shows the correct photo and using OpenHashTab to grab the hash.

![profile](images/profile-eli.png)

## Answer

3CC4E757872A7A9C534AD42BFFAA9F8170A99553
