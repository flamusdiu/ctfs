
# Q#51 - Finder's Keepers (25)

## Challenges

![challenge](../../qn/51.png)

What Source Version is the Finder app on this device?

### Hints

**None**

### Files

**None**

## Solve

Searching AXIOM, you get the following:

![finder](images/finder-version.png)

Of course, just looking at the installed application list would have been faster. I only realized it later.

## Answer

1350.2.10
