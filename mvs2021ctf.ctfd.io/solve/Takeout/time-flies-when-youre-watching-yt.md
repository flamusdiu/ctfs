
# Q#41 - Time flies when you're watching YT (10)

## Challenges

![challenge](../../qn/41.png)

What date was the first YouTube video the user watched uploaded? (Format: month day, year) (Example: Feb 3 2020)

### Hints

**None**

### Files

**None**

## Solve

Source: `Takeout/YouTube and YouTube Music/history/watch-history.html`

![video](images/takeout-video.png)

Checking the YT page:

![upload](images/takeout-video-upload.png)

## Answer

Feb 3, 2021
