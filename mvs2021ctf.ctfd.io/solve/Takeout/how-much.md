
# Q#69 - How much? (50)

## Challenges

![challenge](../../qn/69.png)

What is the price of the belt ?

### Hints

**None**

### Files

**None**

## Solve

Source: `google-takeout/Takeout/Chrome/BrowserHistory.json`

Search for "belt" and find several URLs.

![url](images/takeout-belt.png)

Opening one of these URLs gives the following web page.

![price](images/takeout-belt-price.png)

## Answer

$98.50

