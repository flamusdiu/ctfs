
# Q#37 - You got mail (5)

## Challenges

![challenge](../../qn/37.png)

How many emails were received from notification@service.tiktok.com?

### Hints

**None**

### Files

**None**

## Solve

You can use the mbox viewer at <https://github.com/eneam/mboxviewer/releases/tag/v1.0.3.20>

![mail](images/takeout-mail.png)

## Answer

6
