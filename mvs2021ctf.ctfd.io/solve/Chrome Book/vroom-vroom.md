
# Q#36 - Vroom Vroom (50)

## Challenges

![challenge](../../qn/36.png)

What is the name of the car related theme?

### Hints

**None**

### Files

**None**

## Solve

While in AXIOM, you notice a nice looking car in an image:

![image search](images/chromebook-axiom-image-search.png)

If you locate this image, you find the theme location and example the manifest json:

![theme](images/chromebook-theme.png)

## Answer

Lamborghini Cherry&lt;space&gt;
