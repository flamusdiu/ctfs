
# Q#32 - Time to jam out (10)

## Challenges

![challenge](../../qn/32.png)

How many songs does Eli have downloaded?

### Hints

**None**

### Files

**None**

## Solve

In AXIOM, looking under Media, examining the Audio files, shows 2 files.

![music search](images/chromebook-music-search.png)

Double checking the folder these are stored in `\decrypted\mount\User\MyFiles\Music\`.

![my music](images/chromebook-mymusic.png)

## Answer

2
