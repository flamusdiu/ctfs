
# Q#31 - Key-ty Cat (10)

## Challenges

![challenge](../../qn/31.png)

What are the last five characters of the key for the Tabby Cat extension? 

### Hints

**None**

### Files

**None**

## Solve

Doing a search for `tabby cat` gives some links for a Chrome extension.

![search](images/key-ty-search.png)

Looking at the Extension id, you can look at the manifest here: `chromebook.tar/./decrypted/mount/user/Extensions/mefhakmgclhhfbdadeojlkbllmecialg/2.0.0_0/manifest.json`

![key-ty manifest](images/key-ty-manifest.png)

## Answer

IDAQAB
