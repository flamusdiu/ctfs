
# Q#35 - Repeat customer (25)

## Challenges

![challenge](../../qn/35.png)

What was Eli's top visited site? (Domain Name)

### Hints

**None**

### Files

**None**

## Solve

Using cLEAPP, the Chromebook Top Sites report shows the list of sites where "0" is the top site.

![cleapp](images/repeat-chromebook-top-sites.png)

## Answer

protonmail.com
