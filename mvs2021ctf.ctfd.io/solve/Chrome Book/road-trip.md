
# Q#30 - Road Trip! (5)

## Challenges

![challenge](../../qn/30.png)

What city was Eli's destination in?

### Hints

**None**

### Files

**None**

## Solve

Using the map from [It's About the Journey Not the Destination](its-about-the-journey-not-the-destination.md) question:

![journey map](images/journey-maps.png)

It shows the city.

## Answer

Plattsburgh
