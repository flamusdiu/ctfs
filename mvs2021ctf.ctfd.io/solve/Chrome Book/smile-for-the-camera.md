
# Q#29 - Smile for the camera (5)

## Challenges

![challenge](../../qn/29.png)

What is the MD5 hash of the user's profile photo? 

### Hints

**None**

### Files

**None**

## Solve

Chrome stores the image of the user as an "Avatar" search for that yields:

![avatar](images/chromebook-avatar.png)

## Answer

5ddd4fe0041839deb0a4b0252002127b
