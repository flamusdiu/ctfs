
# Q#33 - Dress for success (10)

## Challenges

![challenge](../../qn/33.png)

In bytes, what is the logical size of this bird's image?

### Hints

**None**

### Files

**None**

## Solve

Looking in the downloads folder, you'll see a `tux.png` "dressed for success"

![ileapp](images/dress-ileapp.png)

![tux](images/dress-tux.png)

There are several methods to get the file size, here's one looking directly at the tar file.

```bash
$ tar -tvf chromebook.tar| grep -i "tux"
-rw-r--r-- 0/0           46791 2021-04-05 03:48 ./decrypted/mount/user/Downloads/tux.png
```

## Answer

46791
