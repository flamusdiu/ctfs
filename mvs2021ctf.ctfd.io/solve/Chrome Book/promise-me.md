
# Q#42 - Promise Me (5)

## Challenges

![challenge](../../qn/42.png)

How many promises does Wickr make?

### Hints

**None**

### Files

**None**

## Solve

Not sure where to start, I just performed a simple search. The search points out a PDF on the Wickr page.

![wickr google search](images/wickr-google-search.png)

![wicker](images/wicker-pdf.png)

### Second Solve

The pdf is also located on the Chromebook in the Downloads folder:

![downloads](images/wicker-downloads.png)

## Answer

9
